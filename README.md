## Установка
```shell
php -r "copy('https://bitbucket.org/zaharov-a/student-codegenerator/downloads/codegenerator-setup.php', 'codegenerator-setup.php');"
php -r "if (hash_file('sha384', 'codegenerator-setup.php') === '3d61a891bd2c17ac04ab111997680e6d2a5d156f0d6480f6793b49493c3384170cf17d78b6690e186e650735bd989a3a') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('codegenerator-setup.php'); } echo PHP_EOL;"
php codegenerator-setup.php
php -r "unlink('codegenerator-setup.php');"
```
