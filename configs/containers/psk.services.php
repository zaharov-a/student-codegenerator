<?php

declare(strict_types=1);

/** @var Psr\Container\ContainerInterface $container ; */
$container->add(
    Ox3a\CodeGenerators\Services\Settings\SettingsInterface::class,
    Ox3a\CodeGenerators\Services\Settings\PskSettings::class
);

$container->add(
    Ox3a\CodeGenerators\Mapping\Services\Builders\MapperBuilderInterface::class,
    $container->get(Ox3a\CodeGenerators\Mapping\Services\Builders\PSKMapperBuilder::class)
);
