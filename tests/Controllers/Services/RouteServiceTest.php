<?php

namespace Ox3a\CodeGenerators\Tests\Controllers\Services;

use Ox3a\Annotation\Analyzer;
use Ox3a\CodeGenerators\Controllers\Services\RouteService;
use Ox3a\CodeGenerators\Tests\Controllers\Services\src\TestModule\Controllers\AdminController;
use Ox3a\CodeGenerators\Tests\Controllers\Services\src\TestModule\Controllers\IndexController;
use Ox3a\Service\CaseService;
use PHPUnit\Framework\TestCase;

class RouteServiceTest extends TestCase
{

    public function testGenerate()
    {
        $root      = realpath(__DIR__);
        $generator = new RouteService(new Analyzer(), new CaseService());

        $data = $generator->generate($root, '/src/*/Controllers/*');

        $expected = [
            __DIR__ . '/src/TestModule/configs/routes.cfg.php'  => [
                'index'              => [
                    'type'    => 'segment',
                    'options' => [
                        'route'       => '/base-url',
                        'constraints' => [],
                        'defaults'    => [
                            '_action'        => 'indexAction',
                            '_actionName'    => 'index',
                            '_controller'    => IndexController::class,
                            '_controllerDir' => 'index',
                            '_moduleName'    => 'TestModule',
                        ],
                    ],
                ],
                'index.Get'          => [
                    'type'    => 'segment',
                    'options' => [
                        'route'       => '/base-url/get/:id',
                        'constraints' => [
                            'id' => '\d+'
                        ],
                        'defaults'    => [
                            '_action'        => 'getAction',
                            '_actionName'    => 'get',
                            '_controller'    => IndexController::class,
                            '_controllerDir' => 'index',
                            '_moduleName'    => 'TestModule',
                        ],
                    ],
                ],
                'admin.Index'        => [
                    'type'    => 'segment',
                    'options' => [
                        'route'       => '/admin/:parentId',
                        'constraints' => [],
                        'defaults'    => [
                            '_action'        => 'indexAction',
                            '_actionName'    => 'index',
                            '_controller'    => AdminController::class,
                            '_controllerDir' => 'admin',
                            '_moduleName'    => 'TestModule',
                        ],
                    ],
                ],
                'admin.Edit'         => [
                    'type'    => 'segment',
                    'options' => [
                        'route'       => '/admin/:parentId/edit/:id',
                        'constraints' => [
                            'id' => '\d+'
                        ],
                        'defaults'    => [
                            '_action'        => 'editAction',
                            '_actionName'    => 'edit',
                            '_controller'    => AdminController::class,
                            '_controllerDir' => 'admin',
                            '_moduleName'    => 'TestModule',
                        ],
                    ],
                ],
                'admin2.admin.Index' => [
                    'type'    => 'segment',
                    'options' => [
                        'route'       => '/admin/:parentId',
                        'constraints' => [
                            'parentId' => '\d+',
                        ],
                        'defaults'    => [
                            '_action'        => 'indexAction',
                            '_actionName'    => 'index',
                            '_controller'    => AdminController::class,
                            '_controllerDir' => 'admin',
                            '_moduleName'    => 'TestModule',
                        ],
                    ],
                ],
                'admin2.admin.Edit'  => [
                    'type'    => 'segment',
                    'options' => [
                        'route'       => '/admin/:parentId/edit/:id',
                        'constraints' => [
                            'parentId' => '\d+',
                            'id'       => '\d+'
                        ],
                        'defaults'    => [
                            '_action'        => 'editAction',
                            '_actionName'    => 'edit',
                            '_controller'    => AdminController::class,
                            '_controllerDir' => 'admin',
                            '_moduleName'    => 'TestModule',
                        ],
                    ],
                ],
            ],
            __DIR__ . '/src/TestModule/configs/granted.cfg.php' => [],
            __DIR__ . '/src/TestModule/configs/acl.cfg.php'     => [],
            __DIR__ . '/src/TestModule/configs/layouts.cfg.php' => [
                AdminController::class => 'admin',
            ],
        ];

        $this->assertEquals($expected, $data);
    }

}
