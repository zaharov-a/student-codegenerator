<?php

namespace Ox3a\CodeGenerators\Tests\Controllers\Services\src\TestModule\Controllers;

use Ox3a\Annotation\Route;

/**
 * @Route("/base-url")
 */
class IndexController
{
    /**
     * @Route("", name="index")
     * @return void
     */
    public function indexAction()
    {
    }

    /**
     * @Route("/get/{id<\d+>}", name="index.Get")
     * @return void
     */
    public function getAction()
    {
    }
}
