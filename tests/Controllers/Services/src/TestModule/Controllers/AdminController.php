<?php

namespace Ox3a\CodeGenerators\Tests\Controllers\Services\src\TestModule\Controllers;

use Ox3a\Annotation\Route;
use Ox3a\Annotation\View\Layout;

/**
 * @Route("/admin/{parentId}")
 * @Route("/admin/{parentId<\d+>}", name="admin2.")
 * @Layout("admin")
 */
class AdminController
{
    /**
     * @Route("", name="admin.Index")
     * @return void
     */
    public function indexAction()
    {
    }

    /**
     * @Route("/edit/{id<\d+>}", name="admin.Edit")
     * @return void
     */
    public function editAction()
    {
    }
}
