<?php
/**
 * {Template_Description_Abstract}
 *
 * @author    Zakharov.A
 * @date      15.10.2022
 * @copyright {Template_Description_Copyrights}
 */

namespace Ox3a\CodeGenerators\Tests\Mapping\Services\Builders;

use DateTimeImmutable;
use Nette\PhpGenerator\PsrPrinter;
use Ox3a\CodeGenerators\Mapping\Services\Builders\HydratorBuilder;
use PHPUnit\Framework\TestCase;

class HydratorBuilderTest extends TestCase
{

    /**
     * @param $properties
     * @param $readonly
     * @param $hydrator
     * @param $entity
     * @param $reference
     * @return void
     * @dataProvider buildDataProvider
     */
    public function testBuild($properties, $readonly, $hydrator, $entity, $reference)
    {
        $builder = new HydratorBuilder();

        $phpFile = $builder->build($properties, $readonly, $hydrator, $entity);

        $printer = new PsrPrinter();

        $this->assertEquals(
            file_get_contents($reference),
            $printer->printFile($phpFile)
        );
    }

    public function buildDataProvider()
    {
        return [
            $this->getCaseTestModel(),
            $this->getCaseTestWithViewonlyModel(),
        ];
    }

    public function testBuildedHydrator()
    {
        $hydrator = new \TestModule\Repositories\Db\TestModel\TestHydrator();

        //
        $entity = new \TestModule\Models\TestModel();
        $hydrator->hydrate($entity, []);

        $this->assertNull($entity->getId());
        $this->assertNull($entity->getName());
        $this->assertNull($entity->getBirthDate());
        $this->assertNull($entity->getCreatedAt());
        $this->assertFalse($entity->isActive());
        $this->assertEquals(36.6, $entity->getTemperature());

        $this->assertEquals(
            [
                'id'          => null,
                'name'        => null,
                'birth_date'  => null,
                'created_at'  => null,
                'is_active'   => 0,
                'temperature' => 36.6,
            ],
            $hydrator->extract($entity)
        );

        //
        $entity = new \TestModule\Models\TestModel();
        $hydrator->hydrate($entity, [
            'id'          => '1',
            'name'        => 'name',
            'birthDate'   => '2020-02-02',
            'createdAt'   => '2020-02-02 00:12:33',
            'active'      => '1',
            'temperature' => '39',
        ]);

        $this->assertEquals(1, $entity->getId());
        $this->assertEquals('name', $entity->getName());
        $this->assertEquals(new DateTimeImmutable('2020-02-02'), $entity->getBirthDate());
        $this->assertEquals(new DateTimeImmutable('2020-02-02 00:12:33'), $entity->getCreatedAt());
        $this->assertTrue($entity->isActive());
        $this->assertEquals(39.0, $entity->getTemperature());

        $this->assertEquals(
            [
                'id'          => 1,
                'name'        => 'name',
                'birth_date'  => '2020-02-02',
                'created_at'  => '2020-02-02 00:12:33',
                'is_active'   => 1,
                'temperature' => 39.0,
            ],
            $hydrator->extract($entity)
        );

        ///
        $hydrator = new \TestModule\Repositories\Db\TestWithViewonlyModel\TestWithViewonlyHydrator();

        //
        $entity = new \TestModule\Models\TestWithViewonlyModel();
        $hydrator->hydrate($entity, []);

        $this->assertNull($entity->getId());
        $this->assertNull($entity->getName());
        $this->assertNull($entity->getBirthDate());
        $this->assertNull($entity->getAge());

        $this->assertEquals(
            [
                'id'          => null,
                'name'        => null,
                'birth_date'  => null,
            ],
            $hydrator->extract($entity)
        );

        //
        $entity = new \TestModule\Models\TestWithViewonlyModel();
        $hydrator->hydrate($entity, [
            'id'          => '1',
            'name'        => 'name',
            'birthDate'   => '2020-02-02 00:12:33',
            'age'         => '33',
        ]);

        $this->assertEquals(1, $entity->getId());
        $this->assertEquals('name', $entity->getName());
        $this->assertEquals(new DateTimeImmutable('2020-02-02 00:12:33'), $entity->getBirthDate());
        $this->assertEquals(33, $entity->getAge());

        $this->assertEquals(
            [
                'id'          => '1',
                'name'        => 'name',
                'birth_date'   => '2020-02-02 00:12:33',
            ],
            $hydrator->extract($entity)
        );
    }

    private function getCaseTestModel()
    {
        /// TestModel
        $properties = [
            'id'          => ['id', 'int'],
            'name'        => ['name', 'string'],
            'birthDate'   => ['birth_date', 'Date'],
            'active'      => ['is_active', 'bool'],
            'createdAt'   => ['created_at', 'DateTime'],
            'temperature' => ['temperature', 'float'],
        ];

        $hydrator = [
            'namespace' => 'TestModule\Repositories\Db\TestModel',
            'shortName' => 'TestHydrator',
            'class'     => 'TestModule\Repositories\Db\TestModel\TestHydrator',
        ];

        $entity = [
            'shortName' => 'TestModel',
            'class'     => \TestModule\Models\TestModel::class,
        ];

        $reference = __DIR__ . '/../../test-data/reference/Repositories/Db/TestModel/TestHydrator.php';

        return [$properties, [], $hydrator, $entity, $reference];
    }

    private function getCaseTestWithViewonlyModel()
    {
        // TestWithViewonlyModel
        $properties = [
            'id'        => ['id', 'int'],
            'name'      => ['name', 'string'],
            'birthDate' => ['birth_date', 'DateTime'],
            'age'       => ['age', 'int'],
        ];

        $readonly = ['age'];

        $hydrator = [
            'namespace' => 'TestModule\Repositories\Db\TestWithViewonlyModel',
            'shortName' => 'TestWithViewonlyHydrator',
            'class'     => 'TestModule\Repositories\Db\TestWithViewonlyModel\TestWithViewonlyHydrator',
        ];

        $entity = [
            'shortName' => 'TestWithViewonlyModel',
            'class'     => \TestModule\Models\TestWithViewonlyModel::class,
        ];

        $reference = __DIR__ . '/../../test-data/reference/Repositories/Db/TestWithViewonlyModel/TestWithViewonlyHydrator.php';

        return [$properties, $readonly, $hydrator, $entity, $reference];
    }
}
