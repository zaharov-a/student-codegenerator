<?php

/**
 * auto generated
 */

namespace TestModule\Repositories\Db\TestModel;

use DateTimeImmutable;
use DateTimeInterface;
use ReflectionClass;
use TestModule\Models\TestModel;

/**
 * Class TestHydrator
 * @package TestModule\Repositories\Db\TestModel
 */
class TestHydrator
{
    private static $reflections = [];

    /**
     * Карта полей
     * @var string[][]
     */
    private $map = [
        'id' => ['id', 'int'],
        'name' => ['name', 'string'],
        'birthDate' => ['birth_date', 'Date'],
        'active' => ['is_active', 'bool'],
        'createdAt' => ['created_at', 'DateTime'],
        'temperature' => ['temperature', 'float'],
    ];

    private static function getReflectionProperty($property)
    {
        if (!isset(self::$reflections[$property])) {
            $reflectionClass = self::getReflectionClass();

            $reflectionProperty = $reflectionClass->getProperty($property);
            $reflectionProperty->setAccessible(true);

            self::$reflections[$property] = $reflectionProperty;
        }

        return self::$reflections[$property];
    }

    private static function getReflectionClass()
    {
        if (!isset(self::$reflections['__CLASS__'])) {
            self::$reflections['__CLASS__'] = new ReflectionClass(TestModel::class);
        }
        return self::$reflections['__CLASS__'];
    }

    /**
     * Заполнить объект данными
     * @param TestModel $object
     * @param array $data
     * @return TestModel
     */
    public function hydrate(TestModel $object, array $data)
    {
        foreach ($this->map as $property => $settings) {
            if (array_key_exists($property, $data)) {
                $value         = $data[$property];
                $hydrateMethod = "create" . ucfirst($settings[1]);

                $this->hydrateProperty($object, $property, $this->{$hydrateMethod}($value));
            }
        }

        return $object;
    }

    /**
     * Извлечь данные из объекта
     * @param TestModel $object
     * @return array
     */
    public function extract(TestModel $object)
    {
        $dbData = [];

        foreach ($this->map as $property => $settings) {
            $field         = $settings[0];
            $extractMethod = "extract" . ucfirst($settings[1]);

            $dbData[$field] = $this->{$extractMethod}($this->extractProperty($object, $property));
        }

        return $dbData;
    }

    /**
     * Заполнить данными свойство объекта
     * @param TestModel $object
     * @param string $property
     * @param mixed $value
     * @return void
     */
    public function hydrateProperty(TestModel $object, $property, $value)
    {
        self::getReflectionProperty($property)->setValue($object, $value);
    }

    /**
     * Извлечь данные из свойства объекта
     * @param TestModel $object
     * @param string $property
     * @return mixed
     */
    public function extractProperty(TestModel $object, $property)
    {
        return self::getReflectionProperty($property)->getValue($object);
    }

    /**
     * Создать строковое значение
     * @param string|null $value
     * @return string|null
     */
    private function createString($value)
    {
        return is_null($value) ? null : ((string)$value);
    }

    /**
     * Извлечь строковое значение
     * @param string|null $value
     * @return string|null
     */
    private function extractString($value)
    {
        return $value;
    }

    /**
     * Создать целочисленное значение
     * @param string|null $value
     * @return int|null
     */
    private function createInt($value)
    {
        return is_null($value) ? null : ((int)$value);
    }

    /**
     * Извлечь целочисленное значение
     * @param int|null $value
     * @return int|null
     */
    private function extractInt($value)
    {
        return $value;
    }

    /**
     * Создать дробное значение
     * @param string|null $value
     * @return float|null
     */
    private function createFloat($value)
    {
        return is_null($value) ? null : ((float)$value);
    }

    /**
     * Извлечь дробное значение
     * @param float|null $value
     * @return float|null
     */
    private function extractFloat($value)
    {
        return $value;
    }

    /**
     * Создать логическое значение
     * @param string|null $value
     * @return bool|null
     */
    private function createBool($value)
    {
        return is_null($value) ? null : ((bool)$value);
    }

    /**
     * Извлечь логическое значение
     * @param bool|null $value
     * @return int|null
     */
    private function extractBool($value)
    {
        return is_null($value) ? null : ((int)(bool)$value);
    }

    /**
     * Создать объект даты
     * @param string|null $value
     * @return DateTimeInterface|null
     */
    private function createDate($value)
    {
        return $value ? new DateTimeImmutable($value) : null;
    }

    /**
     * Извлечь дату
     * @param DateTimeInterface|null $value
     * @return string|null
     */
    private function extractDate($value)
    {
        return $value ? $value->format("Y-m-d") : null;
    }

    /**
     * Создать объект даты со временем
     * @param string|null $value
     * @return DateTimeInterface|null
     */
    private function createDateTime($value)
    {
        return $value ? new DateTimeImmutable($value) : null;
    }

    /**
     * Извлечь дату со временем
     * @param DateTimeInterface|null $value
     * @return string|null
     */
    private function extractDateTime($value)
    {
        return $value ? $value->format("Y-m-d H:i:s") : null;
    }
}
