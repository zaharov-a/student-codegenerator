<?php

/**
 * auto generated
 */

namespace TestModule\Repositories\Db\TestWithViewonlyModel;

use DateTimeImmutable;
use DateTimeInterface;
use ReflectionClass;
use TestModule\Models\TestWithViewonlyModel;

/**
 * Class TestWithViewonlyHydrator
 * @package TestModule\Repositories\Db\TestWithViewonlyModel
 */
class TestWithViewonlyHydrator
{
    private static $reflections = [];

    /**
     * Карта полей
     * @var string[][]
     */
    private $map = [
        'id' => ['id', 'int'],
        'name' => ['name', 'string'],
        'birthDate' => ['birth_date', 'DateTime'],
        'age' => ['age', 'int'],
    ];

    /**
     * Список полей для исключения из записи
     * @var string[]
     */
    private $readonly = ['age'];

    private static function getReflectionProperty($property)
    {
        if (!isset(self::$reflections[$property])) {
            $reflectionClass = self::getReflectionClass();

            $reflectionProperty = $reflectionClass->getProperty($property);
            $reflectionProperty->setAccessible(true);

            self::$reflections[$property] = $reflectionProperty;
        }

        return self::$reflections[$property];
    }

    private static function getReflectionClass()
    {
        if (!isset(self::$reflections['__CLASS__'])) {
            self::$reflections['__CLASS__'] = new ReflectionClass(TestWithViewonlyModel::class);
        }
        return self::$reflections['__CLASS__'];
    }

    /**
     * Заполнить объект данными
     * @param TestWithViewonlyModel $object
     * @param array $data
     * @return TestWithViewonlyModel
     */
    public function hydrate(TestWithViewonlyModel $object, array $data)
    {
        foreach ($this->map as $property => $settings) {
            if (array_key_exists($property, $data)) {
                $value         = $data[$property];
                $hydrateMethod = "create" . ucfirst($settings[1]);

                $this->hydrateProperty($object, $property, $this->{$hydrateMethod}($value));
            }
        }

        return $object;
    }

    /**
     * Извлечь данные из объекта
     * @param TestWithViewonlyModel $object
     * @return array
     */
    public function extract(TestWithViewonlyModel $object)
    {
        $dbData = [];

        foreach ($this->map as $property => $settings) {
            if (in_array($property, $this->readonly)) {
                continue;
            }
            $field         = $settings[0];
            $extractMethod = "extract" . ucfirst($settings[1]);

            $dbData[$field] = $this->{$extractMethod}($this->extractProperty($object, $property));
        }

        return $dbData;
    }

    /**
     * Заполнить данными свойство объекта
     * @param TestWithViewonlyModel $object
     * @param string $property
     * @param mixed $value
     * @return void
     */
    public function hydrateProperty(TestWithViewonlyModel $object, $property, $value)
    {
        self::getReflectionProperty($property)->setValue($object, $value);
    }

    /**
     * Извлечь данные из свойства объекта
     * @param TestWithViewonlyModel $object
     * @param string $property
     * @return mixed
     */
    public function extractProperty(TestWithViewonlyModel $object, $property)
    {
        return self::getReflectionProperty($property)->getValue($object);
    }

    /**
     * Создать строковое значение
     * @param string|null $value
     * @return string|null
     */
    private function createString($value)
    {
        return is_null($value) ? null : ((string)$value);
    }

    /**
     * Извлечь строковое значение
     * @param string|null $value
     * @return string|null
     */
    private function extractString($value)
    {
        return $value;
    }

    /**
     * Создать целочисленное значение
     * @param string|null $value
     * @return int|null
     */
    private function createInt($value)
    {
        return is_null($value) ? null : ((int)$value);
    }

    /**
     * Извлечь целочисленное значение
     * @param int|null $value
     * @return int|null
     */
    private function extractInt($value)
    {
        return $value;
    }

    /**
     * Создать объект даты со временем
     * @param string|null $value
     * @return DateTimeInterface|null
     */
    private function createDateTime($value)
    {
        return $value ? new DateTimeImmutable($value) : null;
    }

    /**
     * Извлечь дату со временем
     * @param DateTimeInterface|null $value
     * @return string|null
     */
    private function extractDateTime($value)
    {
        return $value ? $value->format("Y-m-d H:i:s") : null;
    }
}
