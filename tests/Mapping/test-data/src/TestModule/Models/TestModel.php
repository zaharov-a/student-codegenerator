<?php

namespace TestModule\Models;

use DateTimeImmutable;
use Ox3a\Annotation\Mapping;

/**
 * @Mapping\Table("test_models")
 * @Mapping\Join("test_models2", on="test_models.id=a2.id", alias="a2")
 */
class TestModel
{
    /**
     * @Mapping\Id()
     * @Mapping\Column("id", type="int")
     * @var int|null
     */
    private $id;

    /**
     * @Mapping\Column("CONCAT(name, 'asd')", type="string")
     * @var string
     */
    private $name;

    /**
     * @Mapping\Column("birth_date", type="Date")
     * @var DateTimeImmutable
     */
    private $birthDate;

    /**
     * @Mapping\Column("is_active", type="bool")
     * @var bool
     */
    private $active = false;

    /**
     * @Mapping\Column("created_at", type="DateTime")
     * @var DateTimeImmutable
     */
    private $createdAt;

    /**
     * @Mapping\Column("temperature", type="float", table="a2")
     * @var float
     */
    private $temperature = 36.6;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return TestModel
     */
    public function setName(string $name): TestModel
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return DateTimeImmutable
     */
    public function getBirthDate(): ?DateTimeImmutable
    {
        return $this->birthDate;
    }

    /**
     * @param DateTimeImmutable $birthDate
     * @return TestModel
     */
    public function setBirthDate(DateTimeImmutable $birthDate): TestModel
    {
        $this->birthDate = $birthDate;
        return $this;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->active;
    }

    /**
     * @param bool $active
     * @return TestModel
     */
    public function setActive(bool $active): TestModel
    {
        $this->active = $active;
        return $this;
    }

    /**
     * @return DateTimeImmutable
     */
    public function getCreatedAt(): ?DateTimeImmutable
    {
        return $this->createdAt;
    }

    /**
     * @param DateTimeImmutable $createdAt
     * @return TestModel
     */
    public function setCreatedAt(DateTimeImmutable $createdAt): TestModel
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return float
     */
    public function getTemperature(): float
    {
        return $this->temperature;
    }

    /**
     * @param float $temperature
     * @return TestModel
     */
    public function setTemperature(float $temperature): TestModel
    {
        $this->temperature = $temperature;
        return $this;
    }



}
