<?php

namespace TestModule\Models;

use DateTimeImmutable;
use Ox3a\Annotation\Mapping;

/**
 * @Mapping\Table("test_models")
 */
class TestWithViewonlyModel
{
    /**
     * @Mapping\Id()
     * @Mapping\Column("id", type="int")
     * @var int|null
     */
    private $id;

    /**
     * @Mapping\Column("name", type="string")
     * @var string|null
     */
    private $name;

    /**
     * @Mapping\Column("birth_date", type="DateTime")
     * @var DateTimeImmutable|null
     */
    private $birthDate;

    /**
     * @Mapping\Column("age", type="int")
     * @Mapping\Viewonly()
     * @var int|null
     */
    private $age;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return TestWithViewonlyModel
     */
    public function setName(string $name): TestWithViewonlyModel
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return DateTimeImmutable
     */
    public function getBirthDate(): ?DateTimeImmutable
    {
        return $this->birthDate;
    }

    /**
     * @param DateTimeImmutable $birthDate
     * @return TestWithViewonlyModel
     */
    public function setBirthDate(DateTimeImmutable $birthDate): TestWithViewonlyModel
    {
        $this->birthDate = $birthDate;
        return $this;
    }

    /**
     * @return int
     */
    public function getAge(): ?int
    {
        return $this->age;
    }

    /**
     * @param int $age
     * @return TestWithViewonlyModel
     */
    public function setAge(int $age): TestWithViewonlyModel
    {
        $this->age = $age;
        return $this;
    }

}
