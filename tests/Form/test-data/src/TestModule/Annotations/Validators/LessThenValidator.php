<?php
/**
 * {Template_Description_Abstract}
 *
 * @author    Zakharov.A
 * @date      26.02.2024
 * @copyright {Template_Description_Copyrights}
 */

namespace TestModule\Annotations\Validators;

use Doctrine\Common\Annotations\Annotation;
use Ox3a\Annotation\Form\ValidatorBuilderInterface;

/**
 * @Annotation()
 */
class LessThenValidator implements ValidatorBuilderInterface
{
    private $min;

    /**
     * Validation constructor.
     * @param array $data
     */
    public function __construct($data)
    {
        if (isset($data['value'])) {
            $data['min'] = $data['value'];
            unset($data['value']);
        }

        $this->min = $data['min'];
    }

    public function __toString()
    {
        return "new \\Zend\\Validator\\LessThan(['min'=>{$this->min}])";
    }

}
