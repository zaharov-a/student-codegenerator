<?php
namespace TestModule\Forms;

use TestModule\Models\TestRequestModel;
use Ox3a\Form\Model\FormModel;
use Ox3a\Form\Model;
use Zend\Filter;
use Zend\Validator;


class TestRequestForm extends FormModel
{

    public function init()
    {

        $this->add(
            [
                'name' => "age",
                'attributes' => [
                ],
                'options' => [
                    'escapeAttr' => true,
                ],
                'validators' => [
                    new \Zend\Validator\LessThan(['min'=>5]),
                ],
                'type' => \Ox3a\Form\Model\ElementModel::class,
                'filters' => [
                ],
            ]
        );
        $this->add(
            [
                'type' => \Ox3a\Form\Model\ButtonModel::class,
                'name' => "submit",
                'options' => [
                    'label' => "&nbsp;",
                    'title' => "Сохранить",
                ],
            ]
        );

    }

    public function getDataModel()
    {
        $model = new TestRequestModel();

        foreach ($this->getData() as $field => $value) {
            if (property_exists($model, $field)) {
                $model->$field = $value;
            }
        }

        return $model;
    }

}
