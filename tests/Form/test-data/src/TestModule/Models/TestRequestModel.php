<?php
/**
 * {Template_Description_Abstract}
 *
 * @author    Zakharov.A
 * @date      26.02.2024
 * @copyright {Template_Description_Copyrights}
 */

namespace TestModule\Models;
use Ox3a\Annotation\Form;
use TestModule\Annotations\Validators\LessThenValidator;

class TestRequestModel
{
    /**
     * @Form\Element()
     * @Form\Validator(@LessThenValidator(5))
     * @var int
     */
    public $age;

}
