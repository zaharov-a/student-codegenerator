<?php
/**
 * {Template_Description_Abstract}
 *
 * @author    Zakharov.A
 * @date      07.01.2021
 * @copyright {Template_Description_Copyrights}
 */

namespace Ox3a\CodeGenerators\Controllers\Commands;

use Composer\Autoload\ClassLoader;
use Ox3a\CodeGenerators\Controllers\Services\RouteService;
use Ox3a\CodeGenerators\Services\PhpGenerator\Dumper;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class RoutesCommand extends Command
{
    protected static $defaultName = 'controller:routes';

    /**
     * @var string
     */
    private $root;

    private $controllerNamespace = 'Controllers';

    /**
     * @var ClassLoader
     */
    private $loader;

    /**
     * @var RouteService
     */
    private $routeService;

    /**
     * @param ClassLoader $loader
     * @param RouteService $routeService
     */
    public function __construct(ClassLoader $loader, RouteService $routeService)
    {
        $this->loader = $loader;
        $this->routeService = $routeService;

        parent::__construct();
    }

    protected function configure()
    {
        $this->addArgument('root', InputArgument::REQUIRED, 'Корень проекта');

        $this->setDescription('Сгенерировать конфиги маршрутов и доступов к ним');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->root = realpath($input->getArgument('root'));
        $pattern    = sprintf('/src/*Module/%s/*', $this->controllerNamespace);

        $autoload = $this->root . '/vendor/composer/autoload_psr4.php';
        if (is_file($autoload)) {
            $map = require $autoload;
            if (($baseNamespaces = getenv('CODEGENERATOR_BASE_NAMESPACE'))) {
                $baseNamespaces = explode(',', $baseNamespaces);
            } else {
                $output->write('не установлена переменная окружения CODEGENERATOR_BASE_NAMESPACE ');
                $output->writeln('берется значение по умолчанию');

                $baseNamespaces = ['Ox3a'];
            }

            if ($baseNamespaces) {
                foreach ($map as $namespace => $path) {
                    $load = false;
                    foreach ($baseNamespaces as $baseNamespace) {
                        if (strpos($namespace, $baseNamespace) === 0) {
                            $load = true;
                            break;
                        }
                    }
                    if ($load) {
                        $this->loader->setPsr4($namespace, $path);
                    }
                }
            }
        }

        $data = $this->routeService->generate($this->root, $pattern, $this->controllerNamespace);
        foreach ($data as $configPath => $content) {
            $this->saveFile($configPath, $content);
        }

        $output->writeln('Ok');

        return self::SUCCESS;
    }

    private function saveFile($name, $data)
    {
        if ($data) {
            file_put_contents($name, "<?php\n\nreturn " . (new Dumper())->dump($data) . ";\n");
        } else {
            if (is_file($name)) {
                unlink($name);
            }
        }
    }
}
