<?php

namespace Ox3a\CodeGenerators\Controllers\Services;

use Ox3a\Annotation\Acl;
use Ox3a\Annotation\Analyzer;
use Ox3a\Annotation\DbTransaction;
use Ox3a\Annotation\IsGranted;
use Ox3a\Annotation\Route;
use Ox3a\Annotation\View\Layout;
use Ox3a\Service\CaseService;
use ReflectionClass;

class RouteService
{

    private $root;

    /**
     * @var Analyzer
     */
    private $analyzer;

    /**
     * @var CaseService
     */
    private $caseService;

    /**
     * @var mixed|string
     */
    private $controllerNamespace;

    /**
     * @param Analyzer $analyzer
     * @param CaseService $caseService
     */
    public function __construct(Analyzer $analyzer, CaseService $caseService)
    {
        $this->analyzer    = $analyzer;
        $this->caseService = $caseService;
    }

    /**
     * @throws \ReflectionException
     * @throws \Doctrine\Common\Annotations\AnnotationException
     */
    public function generate($root, $pattern, $controllerNamespace = 'Controllers')
    {
        $analyzer = $this->analyzer;

        $fileContents = [];

        $this->root = $root;

        $this->controllerNamespace = $controllerNamespace;

        $controllerList = $this->getControllerList($root . $pattern);

        foreach ($controllerList as $moduleDir => $controllers) {
            $routes    = [];
            $isGranted = [];
            $acl       = [];
            $layouts   = [];
            foreach ($controllers as $controller) {
                $reflection = new ReflectionClass($controller['class']);
                if ($reflection->isAbstract()) {
                    continue;
                }

                $annotations = $analyzer->run($controller['class']);

                $controllerClassName = $controller['class'];

                if (($layoutOptions = $this->getControllerLayout($annotations))) {
                    $layouts[$controllerClassName] = $layoutOptions;
                }

                $routes    += $this->getControllerRoutes($moduleDir, $controller, $annotations);
                $isGranted += $this->getControllerGrants($controller, $annotations);
                $acl       += $this->getControllerAcl($controller, $annotations);
            }

            $fileContents[$moduleDir . '/configs/routes.cfg.php']  = $routes;
            $fileContents[$moduleDir . '/configs/granted.cfg.php'] = $isGranted;
            $fileContents[$moduleDir . '/configs/acl.cfg.php']     = $acl;
            $fileContents[$moduleDir . '/configs/layouts.cfg.php'] = $layouts;
        }

        return $fileContents;
    }

    private function getControllerList($pattern): array
    {
        $glob = glob($pattern);
        $list = [];

        foreach ($glob as $file) {
            if (is_dir($file)) {
                $list = array_merge_recursive($list, $this->getControllerList($file . '/*'));
            } else {
                $info = $this->getControllerInfo($file);

                if (!isset($list[$info['dir']])) {
                    $list[$info['dir']] = [];
                }

                $list[$info['dir']][] = $info;
            }
        }

        return $list;
    }

    private function getControllerInfo($file): array
    {
        require_once $file;

        // @see https://stackoverflow.com/questions/7153000/get-class-name-from-file
        $tokens    = token_get_all(file_get_contents($file));
        $namespace = $className = '';

        $namespaceToken = defined('T_NAME_QUALIFIED') ? T_NAME_QUALIFIED : T_STRING;
//        exit(sprintf('<pre>%s</pre>', print_r([__FILE__, __LINE__, $namespaceToken], true)));
        foreach ($tokens as $i => $token) {
            $tokenId = isset($token[0]) ? $token[0] : '';
            switch ($tokenId) {
                default:
                    break;

                case T_CLASS:
                    if (!$className && is_array($tokens[$i + 2])) {
                        $className = $tokens[$i + 2][1];
                        break 2;
                    }
                    // for ($j = $i + 1; $j < count($tokens); $j++) {
                    //     if ($tokens[$j] === '{') {
                    //         $className = $tokens[$i + 2][1];
                    //     }
                    // }
                    break;

                case T_NAMESPACE:
                    for ($j = $i + 1; $j < count($tokens); $j++) {
                        if ($tokens[$j][0] === $namespaceToken) {
                            $namespace .= '\\' . $tokens[$j][1];
                        } else {
                            if ($tokens[$j] === '{' || $tokens[$j] === ';') {
                                break;
                            }
                        }
                    }
                    break;
            }
        }

        if (!$className || !$namespace) {
            print_r([$namespace, $className, $file]);
            throw new \RuntimeException('Обнаружился пустой класс');
        }

        $fileName = str_replace([$this->root . '/src/', '.php'], '', $file);

        $parts = explode('/', $fileName);

        return [
            'dir'   => $this->root . "/src/{$parts[0]}",
            'file'  => $file,
            'class' => trim("{$namespace}\\{$className}", '\\'),
        ];
    }

    /**
     * @param array $controller
     * @param array $annotations
     * @return array
     */
    private function getControllerGrants(array $controller, array $annotations)
    {
        $class     = $annotations['class'];
        $isGranted = [];

        if (isset($class[IsGranted::class])) {
            $isGranted[$controller['class']] = [
                '__construct' => [],
            ];
            foreach ($class[IsGranted::class] as $data) {
                $isGranted[$controller['class']]['__construct'][] = $data;
            }
        }

        foreach ($annotations['methods'] as $actionName => $action) {
            $extraData = [
                '_action'     => $actionName,
                '_controller' => $controller['class'],
            ];

            if (isset($action[IsGranted::class])) {
                foreach ($action[IsGranted::class] as $data) {
                    if (!isset($isGranted[$extraData['_controller']])) {
                        $isGranted[$extraData['_controller']] = [];
                    }
                    if (!isset($isGranted[$extraData['_controller']][$extraData['_action']])) {
                        $isGranted[$extraData['_controller']][$extraData['_action']] = [];
                    }
                    $isGranted[$extraData['_controller']][$extraData['_action']][] = $data;
                }
            }
        }

        return $isGranted;
    }

    /**
     * @param array $controller
     * @param array $annotations
     * @return array
     */
    private function getControllerAcl(array $controller, array $annotations)
    {
        $class = $annotations['class'];
        $acl   = [];

        if (isset($class[Acl::class])) {
            $acl[$controller['class']] = [
                '__construct' => [],
            ];
            foreach ($class[Acl::class] as $data) {
                $acl[$controller['class']]['__construct'][] = $data;
            }
        }

        foreach ($annotations['methods'] as $actionName => $action) {
            $extraData = [
                '_action'     => $actionName,
                '_controller' => $controller['class'],
            ];

            if (isset($action[Acl::class])) {
                foreach ($action[Acl::class] as $data) {
                    if (!isset($acl[$extraData['_controller']])) {
                        $acl[$extraData['_controller']] = [];
                    }
                    if (!isset($acl[$extraData['_controller']][$extraData['_action']])) {
                        $acl[$extraData['_controller']][$extraData['_action']] = [];
                    }
                    $acl[$extraData['_controller']][$extraData['_action']][] = $data;
                }
            }
        }

        return $acl;
    }

    /**
     * @param array $annotations
     * @return string|null
     */
    private function getControllerLayout(array $annotations): ?string
    {
        $class = $annotations['class'];
        if (isset($class[Layout::class])) {
            return $class[Layout::class][0]['layout'];
        }
        return null;
    }

    /**
     * @param string $moduleDir
     * @param array $annotations
     * @return array
     */
    private function getControllerRoutes(string $moduleDir, array $controller, array $annotations): array
    {
        $class       = $annotations['class'];
        $caseService = $this->caseService;

        $routes = [];

        if (isset($class[Route::class])) {
            $bases = array_map(function ($base) {
                return [
                    'name'    => $base['name'],
                    'options' => $base['params']['options'],
                ];
            }, $class[Route::class]);
        } else {
            $bases = [
                [
                    'name'    => '',
                    'options' => [
                        'route'       => '',
                        'constraints' => [],
                        'defaults'    => [],
                    ],
                ],
            ];
        }

        foreach ($bases as $base) {
            foreach ($annotations['methods'] as $actionName => $action) {
                $module = basename($moduleDir);
                $part   = explode(
                    '\\',
                    str_replace(
                        'Controller',
                        '',
                        preg_replace(
                            "/(.+){$module}\\\\{$this->controllerNamespace}\\\\(.+)$/",
                            '$2',
                            $controller['class']
                        )
                    )
                );

                $extraData = [
                    '_action'        => $actionName,
                    '_actionName'    => $caseService->toKebabCase(
                        str_replace('Action', '', $actionName)
                    ),
                    '_controller'    => $controller['class'],
                    '_controllerDir' => implode(
                        '/',
                        array_map(
                            [
                                $caseService,
                                'toKebabCase',
                            ],
                            $part
                        )
                    ),
                    '_moduleName'    => $module,
                ];
                if (isset($action[DbTransaction::class])) {
                    $extraData['_dbTransaction'] = true;
                }
                if (isset($action[Route::class])) {
                    foreach ($action[Route::class] as $route) {
                        $route['name']                             = $base['name'] . $route['name'];
                        $route['params']['options']['route']       = $base['options']['route'] . $route['params']['options']['route'];
                        $route['params']['options']['constraints'] = $base['options']['constraints'] + $route['params']['options']['constraints'];
                        $route['params']['options']['defaults']    = $base['options']['defaults'] + $route['params']['options']['defaults'];

                        $routes[$route['name']] = $route['params'];

                        foreach ($extraData as $key => $value) {
                            $routes[$route['name']]['options']['defaults'][$key] = $value;
                        }
                    }
                }
            }
        }

        return $routes;
    }
}
