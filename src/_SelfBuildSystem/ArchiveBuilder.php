<?php
/**
 * {Template_Description_Abstract}
 *
 * @author    Zakharov.A
 * @date      02.10.2022
 * @copyright {Template_Description_Copyrights}
 */

namespace Ox3a\CodeGenerators\_SelfBuildSystem;

use CallbackFilterIterator;
use FilesystemIterator;
use Phar;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use Composer\Script\Event;

class ArchiveBuilder
{
    public static function build(Event $event)
    {
        $appVersion = getenv('APP_VERSION') ?: 'development';

        $args = $event->getArguments();
        $tag  = $args[0] ?? null;

        $projectDir = realpath(__DIR__ . '/../..');
        $fileName   = $tag ? "codegenerator.{$tag}.phar" : 'codegenerator.phar';

        $ignorePaths = [
            "{$projectDir}/.idea",
            "{$projectDir}/.git",
            "{$projectDir}/build",
            "{$projectDir}/tests",
            "{$projectDir}/bin/codegenerator-setup.php",
        ];

        $phar = new Phar("{$projectDir}/build/{$fileName}", 0, $fileName);

        $phar->buildFromIterator(
            new CallbackFilterIterator(
                new RecursiveIteratorIterator(
                    new RecursiveDirectoryIterator($projectDir, FilesystemIterator::SKIP_DOTS)
                ),
                function (\SplFileInfo $current, $key, $iterator) use ($ignorePaths) {
                    return !in_array($current->getPath(), $ignorePaths);
                }
            ),
            $projectDir
        );

        $defaultStub = $phar->createDefaultStub('bin/codegenerator.php');
        $stub        = "#!/usr/bin/env php\n"
            . str_replace("<?php", "<?php\ndefine('APP_VERSION', '{$appVersion}');\n", $defaultStub);

        $phar->setStub($stub);

        $phar->compressFiles(Phar::GZ);
    }
}
