<?php
/**
 * {Template_Description_Abstract}
 *
 * @author    Zakharov.A
 * @date      01.03.2023
 * @copyright {Template_Description_Copyrights}
 */

namespace Ox3a\CodeGenerators\Form\Models\Validators;

use Ox3a\CodeGenerators\Models\ClassNameModel;
use Ox3a\CodeGenerators\Models\CodeStringModel;

class RegexValidator implements ValidatorBuilderInterface
{
    public function build(array $params, array $annotations): array
    {
        if (empty($params['pattern'])) {
            throw new \InvalidArgumentException('Не указан паттерн');
        }

        $params['pattern'] = new CodeStringModel('"' . $params['pattern'] . '"');

        return [
            'name'    => new ClassNameModel('Zend\Validator\Regex'),
            'options' => $params,
        ];
    }

}
