<?php

namespace Ox3a\CodeGenerators\Form\Models\Validators;

use Ox3a\CodeGenerators\Models\ClassNameModel;

class GreaterThanValidator implements ValidatorBuilderInterface
{
    public function build(array $params, array $annotations): array
    {
        return [
            'name'    => new ClassNameModel("Zend\Validator\GreaterThan"),
            'options' => $params,
        ];
    }

}
