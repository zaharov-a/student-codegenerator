<?php
/**
 * {Template_Description_Abstract}
 *
 * @author    Zakharov.A
 * @date      11.02.2021
 * @copyright {Template_Description_Copyrights}
 */

namespace Ox3a\CodeGenerators\Form\Models\Validators;


use Ox3a\CodeGenerators\Models\ClassNameModel;

class LengthValidator implements ValidatorBuilderInterface
{
    public function build(array $params, array $annotations): array
    {
        return [
            'name'    => new ClassNameModel('Zend\Validator\StringLength'),
            'options' => $params,
        ];
    }


}
