<?php
/**
 * {Template_Description_Abstract}
 *
 * @author    Zakharov.A
 * @date      05.01.2021
 * @copyright {Template_Description_Copyrights}
 */

namespace Ox3a\CodeGenerators\Form\Models\Validators;

use Ox3a\CodeGenerators\Models\ClassNameModel;

class Factory
{
    public function get(string $name): ValidatorBuilderInterface
    {
        $map = [
            'date'        => DateValidator::class,
            'length'      => LengthValidator::class,
            'regex'       => RegexValidator::class,
            'email'       => new ClassNameModel("Zend\Validator\EmailAddress"),
            'digits'      => new ClassNameModel("Zend\Validator\Digits"),
            'greaterThan' => GreaterThanValidator::class,
        ];

        $factoryClass = $map[$name];

        if ($factoryClass instanceof ClassNameModel) {
            return new SimpleValidator($factoryClass);
        }

        return new $factoryClass();
    }
}
