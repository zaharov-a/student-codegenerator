<?php
/**
 * {Template_Description_Abstract}
 *
 * @author    Zakharov.A
 * @date      05.01.2021
 * @copyright {Template_Description_Copyrights}
 */

namespace Ox3a\CodeGenerators\Form\Models\Elements;

use Ox3a\Annotation\Form;
use Ox3a\CodeGenerators\Models\ClassNameModel;
use Ox3a\Form\Model\HiddenModel;

class Factory
{

    /**
     * @var array<string, ClassNameModel|class-string>
     */
    protected $map = [];

    /**
     * Factory constructor.
     */
    public function __construct()
    {
        $this->map = [
            'hidden'        => new ClassNameModel(HiddenModel::class),
            'checkbox'      => CheckboxElement::class,
            'text'          => TextElement::class,
            'textarea'      => TextareaElement::class,
            'date'          => DateElement::class,
            'file'          => FileElement::class,
            'password'      => PasswordElement::class,
            'select'        => SelectElement::class,
            'multiCheckbox' => MultiCheckbox::class,
            'radioGroup'    => RadioGroupElement::class,
            'email'         => EmailElement::class,
            'phone'         => PhoneElement::class,
            'separator'     => SeparatorElement::class,
        ];
    }

    public function get($name, $annotations)
    {
        $elementOptions = $annotations[Form\Element::class][0];
        $elementFactory = $this->getElement($elementOptions['type']);

        $element = $this->collectValidators(
            [
                'name'       => $name,
                'attributes' => $this->collectAttributes($annotations, $elementOptions),
                'options'    => $this->collectOptions($annotations, $elementOptions),
                'filters'    => $this->collectFilters($annotations),
            ],
            $annotations
        );

        if ($elementFactory instanceof ElementBuilderInterface) {
            return $elementFactory->build($element, $annotations);
        }

        $element['type'] = $elementFactory;
        return $element;
    }

    public function getElement($type)
    {
        if (empty($this->map[$type])) {
            throw new \RuntimeException("Нет строителя для элемента: {$type}");
        }
        $factory = $this->map[$type];

        return is_string($factory)
            ? new $factory()
            : $factory;
    }

    /**
     * @param array $annotations
     * @param array $elementOptions
     * @return array
     */
    private function collectAttributes(array $annotations, array $elementOptions): array
    {
        $attributes = array_diff_key(
            array_column($annotations[Form\Attribute::class] ?? [], 'value', 'name'),
            array_flip(['options'])
        );

        if (isset($elementOptions['placeholder'])) {
            $attributes['placeholder'] = $elementOptions['placeholder'];
        }

        return $attributes;
    }

    /**
     * @param array $annotations
     * @param array $elementOptions
     * @return array
     */
    private function collectOptions(array $annotations, array $elementOptions): array
    {
        $options = array_column($annotations[Form\Option::class] ?? [], 'value', 'name');

        if (isset($elementOptions['label'])) {
            $options['label'] = $elementOptions['label'];
        }

        return $options;
    }

    /**
     * @param array $annotations
     * @return array
     */
    private function collectFilters(array $annotations): array
    {
        $filterFactory = new \Ox3a\CodeGenerators\Form\Models\Filters\Factory();

        $filters = [];
        foreach ($annotations[Form\Filter::class] ?? [] as $filterOptions) {
            if ($filterOptions instanceof Form\FilterBuilderInterface) {
                $filters[] = $filterOptions;
            } else {
                $filters[] = $filterFactory->get($filterOptions['name'])
                    ->build($filterOptions['params'], $annotations);
            }
        }

        return $filters;
    }

    private function collectValidators(array $element, array $annotations): array
    {
        $validatorFactory = new \Ox3a\CodeGenerators\Form\Models\Validators\Factory();

        foreach ($annotations[Form\Validator::class] ?? [] as $validatorOptions) {
            if ($validatorOptions instanceof Form\ValidatorBuilderInterface) {
                $element['validators'][] = $validatorOptions;
            } elseif ($validatorOptions['name'] == 'required') {
                $element['attributes']['required'] = true;
            } else {
                $element['validators'][] = $validatorFactory->get($validatorOptions['name'])
                    ->build($validatorOptions['params'], $annotations);
            }
        }

        return $element;
    }

}
