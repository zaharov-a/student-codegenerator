<?php

namespace Ox3a\CodeGenerators\Form\Models\Elements;

use Ox3a\CodeGenerators\Models\ClassNameModel;
use Ox3a\Form\Model\SeparatorModel;

class SeparatorElement implements ElementBuilderInterface
{
    public function build(array $data, array $annotations): array
    {
        return [
            'name' => $data['name'],
            'type' => new ClassNameModel(SeparatorModel::class),
        ];
    }

}
