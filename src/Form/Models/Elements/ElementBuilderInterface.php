<?php
/**
 * {Template_Description_Abstract}
 *
 * @author    Zakharov.A
 * @date      05.01.2021
 * @copyright {Template_Description_Copyrights}
 */

namespace Ox3a\CodeGenerators\Form\Models\Elements;

interface ElementBuilderInterface
{
    public function build(array $data, array $annotations): array;
}
