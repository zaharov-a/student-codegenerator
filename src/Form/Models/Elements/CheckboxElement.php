<?php

namespace Ox3a\CodeGenerators\Form\Models\Elements;

use Ox3a\CodeGenerators\Models\ClassNameModel;
use Ox3a\Form\Model\CheckboxModel;
use Ox3a\Form\Model\RadioGroupModel;

class CheckboxElement implements ElementBuilderInterface
{
    public function build(array $data, array $annotations): array
    {
        $data['type'] = new ClassNameModel(RadioGroupModel::class);

        foreach (['attributes', 'filters', 'validators', 'options'] as $key) {
            if (!isset($data[$key])) {
                $data[$key] = [];
            }
        }

        $data['options']['options'] = [
            1 => 'Да',
            0 => 'Нет',
        ];

        $data['options']['escapeAttr'] = true;

        return $data;
    }

}
