<?php
/**
 * {Template_Description_Abstract}
 *
 * @author    Zakharov.A
 * @date      21.03.2021
 * @copyright {Template_Description_Copyrights}
 */

namespace Ox3a\CodeGenerators\Form\Models\Elements;


use Ox3a\CodeGenerators\Models\ClassNameModel;
use Ox3a\Form\Model\ElementModel;
use Zend\Filter;
use Zend\Validator;

class EmailElement implements ElementBuilderInterface
{
    public function build(array $data, array $annotations): array
    {
        $data['type'] = new ClassNameModel(ElementModel::class);

        foreach (['attributes', 'filters', 'validators', 'options'] as $key) {
            if (!isset($data[$key])) {
                $data[$key] = [];
            }
        }

        $data['attributes']['type'] = 'email';

        $data['options']['escapeAttr'] = true;

        $data['filters'][]    = ['name' => new ClassNameModel(Filter\StringTrim::class)];
        $data['validators'][] = ['name' => new ClassNameModel(Validator\EmailAddress::class)];

        return $data;
    }


}
