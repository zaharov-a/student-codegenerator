<?php
/**
 * {Template_Description_Abstract}
 *
 * @author    Zakharov.A
 * @date      07.08.2021
 * @copyright {Template_Description_Copyrights}
 */

namespace Ox3a\CodeGenerators\Form\Models\Elements;
use Ox3a\CodeGenerators\Models\ClassNameModel;
use Ox3a\Form\Model\MultiCheckboxModel;
use Zend\Filter;
use Zend\Validator;

class MultiCheckbox implements ElementBuilderInterface
{
    public function build(array $data, array $annotations): array
    {
        $data['type'] = new ClassNameModel(MultiCheckboxModel::class);

        foreach (['attributes', 'filters', 'validators', 'options'] as $key) {
            if (!isset($data[$key])) {
                $data[$key] = [];
            }
        }

        $data['options']['escapeAttr'] = true;

        return $data;
    }

}
