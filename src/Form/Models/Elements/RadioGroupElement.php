<?php
/**
 * {Template_Description_Abstract}
 *
 * @author    Zakharov.A
 * @date      28.03.2021
 * @copyright {Template_Description_Copyrights}
 */

namespace Ox3a\CodeGenerators\Form\Models\Elements;

use Ox3a\Annotation\Form\Attribute;
use Ox3a\CodeGenerators\Models\ClassNameModel;
use Ox3a\CodeGenerators\Models\CodeStringModel;
use Ox3a\Form\Model\RadioGroupModel;

class RadioGroupElement implements ElementBuilderInterface
{

    public function build(array $data, array $annotations): array
    {
        $data['type'] = new ClassNameModel(RadioGroupModel::class);

        foreach (['attributes', 'filters', 'validators', 'options'] as $key) {
            if (!isset($data[$key])) {
                $data[$key] = [];
            }
        }
        if (!empty($annotations[Attribute::class])) {
            foreach ($annotations[Attribute::class] as $annotation) {
                if ($annotation['name'] == 'options') {
                    $options = $annotation['value'];
                    if (is_string($options)) {
                        $options = new CodeStringModel("\$this->getOption('{$options}')");
                    }
                    $data['options']['options'] = $options;
                    break;
                }
            }
        }

        $data['options']['escapeAttr'] = true;

        return $data;
    }
}
