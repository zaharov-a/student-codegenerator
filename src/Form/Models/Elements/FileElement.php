<?php
/**
 * {Template_Description_Abstract}
 *
 * @author    Zakharov.A
 * @date      21.03.2021
 * @copyright {Template_Description_Copyrights}
 */

namespace Ox3a\CodeGenerators\Form\Models\Elements;


use Ox3a\CodeGenerators\Models\ClassNameModel;
use Ox3a\Form\Model\ElementModel;
use Ox3a\Form\Model\FileModel;

class FileElement implements ElementBuilderInterface
{
    public function build(array $data, array $annotations): array
    {
        $data['type'] = new ClassNameModel(FileModel::class);

        return $data;
    }


}

