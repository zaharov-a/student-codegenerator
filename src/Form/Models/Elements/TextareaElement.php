<?php
/**
 * {Template_Description_Abstract}
 *
 * @author    Zakharov.A
 * @date      21.03.2021
 * @copyright {Template_Description_Copyrights}
 */

namespace Ox3a\CodeGenerators\Form\Models\Elements;

use Ox3a\Annotation\Form\Attribute;
use Ox3a\Annotation\Form\Option;
use Ox3a\CodeGenerators\Models\ClassNameModel;
use Ox3a\Form\Model\TextAreaModel;

class TextareaElement implements ElementBuilderInterface
{
    public function build(array $data, array $annotations): array
    {
        $data['type'] = new ClassNameModel(TextAreaModel::class);

        foreach (['attributes', 'filters', 'validators', 'options'] as $key) {
            if (!isset($data[$key])) {
                $data[$key] = [];
            }
        }

        $data['options']['escapeAttr'] = $this->isEscapeAttr($annotations);

        return $data;
    }

    private function isEscapeAttr(array $annotations): bool
    {
        foreach ($annotations[Attribute::class] ?? [] as $annotation) {
            if ($annotation['name'] == 'escapeAttr') {
                return $annotation['value'];
            }
        }
        foreach ($annotations[Option::class] ?? [] as $annotation) {
            if ($annotation['name'] == 'escapeAttr') {
                return $annotation['value'];
            }
        }

        return true;
    }

}
