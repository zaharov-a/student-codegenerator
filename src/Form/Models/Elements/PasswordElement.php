<?php
/**
 * {Template_Description_Abstract}
 *
 * @author    Zakharov.A
 * @date      05.01.2021
 * @copyright {Template_Description_Copyrights}
 */

namespace Ox3a\CodeGenerators\Form\Models\Elements;


use Ox3a\CodeGenerators\Models\ClassNameModel;
use Ox3a\Form\Model\ElementModel;

class PasswordElement implements ElementBuilderInterface
{
    public function build(array $data, array $annotations): array
    {
        $data['type'] = new ClassNameModel(ElementModel::class);

        foreach (['attributes', 'filters', 'validators', 'options'] as $key) {
            if (!isset($data[$key])) {
                $data[$key] = [];
            }
        }

        $data['options']['escapeAttr'] = true;

        $data['attributes']['type'] = 'password';

        return $data;
    }


}
