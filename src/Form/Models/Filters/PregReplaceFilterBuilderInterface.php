<?php

namespace Ox3a\CodeGenerators\Form\Models\Filters;

use Nette\PhpGenerator\Literal;
use Ox3a\CodeGenerators\Models\ClassNameModel;

class PregReplaceFilterBuilderInterface implements FilterBuilderInterface
{
    public function build(array $params, array $annotations): array
    {
        if (!array_key_exists('pattern', $params)) {
            throw new \RuntimeException('Укажите паттерн для поиска');
        }
        $result = [
            'name'    => new ClassNameModel("Zend\Filter\PregReplace"),
            'options'=>[
                'pattern' => new Literal("'{$params['pattern']}'")
            ],
        ];

        if (array_key_exists('replace', $params)) {
            $result['options']['replace'] = new Literal("'{$params['replace']}'");
        }

        return $result;
    }

}
