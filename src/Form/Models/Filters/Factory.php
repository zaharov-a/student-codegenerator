<?php
/**
 * {Template_Description_Abstract}
 *
 * @author    Zakharov.A
 * @date      05.01.2021
 * @copyright {Template_Description_Copyrights}
 */

namespace Ox3a\CodeGenerators\Form\Models\Filters;

use Ox3a\CodeGenerators\Models\ClassNameModel;

class Factory
{
    public function get(string $name): FilterBuilderInterface
    {
        $map = [
            'stringTrim'   => new ClassNameModel("Zend\Filter\StringTrim"),
            'htmlEntities' => new ClassNameModel("Zend\Filter\HtmlEntities"),
            'pregReplace'  => new PregReplaceFilterBuilderInterface(),
            'int'          => new ClassNameModel("Zend\Filter\ToInt"),
        ];

        $factory = $map[$name];

        if ($factory instanceof ClassNameModel) {
            return new SimpleFilterBuilderInterface($factory);
        }

        return $factory;
    }
}
