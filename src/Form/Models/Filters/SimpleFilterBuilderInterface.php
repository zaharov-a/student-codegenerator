<?php
/**
 * {Template_Description_Abstract}
 *
 * @author    Zakharov.A
 * @date      11.02.2021
 * @copyright {Template_Description_Copyrights}
 */

namespace Ox3a\CodeGenerators\Form\Models\Filters;


use Ox3a\CodeGenerators\Models\ClassNameModel;

class SimpleFilterBuilderInterface implements FilterBuilderInterface
{
    /**
     * @var ClassNameModel
     */
    private $className;


    /**
     * SimpleFilter constructor.
     * @param ClassNameModel $className
     */
    public function __construct(ClassNameModel $className)
    {
        $this->className = $className;
    }


    public function build(array $params, array $annotations): array
    {
        return [
            'name' => $this->className,
        ];
    }


}
