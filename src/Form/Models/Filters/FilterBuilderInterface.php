<?php
/**
 * {Template_Description_Abstract}
 *
 * @author    Zakharov.A
 * @date      11.02.2021
 * @copyright {Template_Description_Copyrights}
 */

namespace Ox3a\CodeGenerators\Form\Models\Filters;


interface FilterBuilderInterface
{
    public function build(array $params, array $annotations): array;
}
