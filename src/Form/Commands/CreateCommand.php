<?php
/**
 * {Template_Description_Abstract}
 *
 * @author    Zakharov.A
 * @date      28.12.2020
 * @copyright {Template_Description_Copyrights}
 */

namespace Ox3a\CodeGenerators\Form\Commands;

use Ox3a\CodeGenerators\Form\Services\FormCreator;
use RuntimeException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class CreateCommand extends Command
{
    protected static $defaultName = 'form:create';

    protected function configure()
    {
        $this->addArgument('path', InputArgument::REQUIRED, 'Путь до модели');
        $defaultTypeSuffix = getenv('CODEGENERATOR_FORM_TYPE_SUFFIX') ?: "Model";
        $this->addOption(
            "typeSuffix",
            null,
            InputOption::VALUE_OPTIONAL,
            "Тип: Entity, Model, etc",
            $defaultTypeSuffix
        );
        $defaultTypeNamespace = getenv('CODEGENERATOR_FORM_TYPE_NAMESPACE') ?: "Models";
        $this->addOption(
            "typeNamespace",
            null,
            InputOption::VALUE_OPTIONAL,
            "Namespace типа: Entities, Models, etc",
            $defaultTypeNamespace
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $filePath     = $input->getArgument('path');
        $fullFilePath = realpath($filePath);
        $base         = mb_substr($fullFilePath, 0, -mb_strlen($filePath));

        require_once $fullFilePath;

        $classList = get_declared_classes();

        $creator = new FormCreator();

        $creator->setTypeSuffix($input->getOption('typeSuffix'));
        $creator->setTypeNamespace($input->getOption('typeNamespace'));

        $result = $creator->create(end($classList));

        $formsDir = $creator->getFormsDir($fullFilePath);

        if (!is_dir($formsDir)) {
            mkdir($formsDir, 0777, true);
        }

        $formFilePath = $formsDir . '/' . $result[0] . '.php';

        if ($formFilePath == $fullFilePath) {
            throw new RuntimeException('Попытка перезаписать исходник');
        }

        file_put_contents($formFilePath, $result[1]);

        $output->writeln(sprintf("Создана форма: %s", mb_substr($formFilePath, mb_strlen($base))));

        return self::SUCCESS;
    }

}
