<?php
/** @var \Ox3a\CodeGenerators\Form\Services\FormCreator $this */
$printService = $this->getPrintService();
echo '<?php';
?>

namespace <?php echo $this->getNamespace() ?>;

use <?php echo $this->getModelClassName() ?>;
use Ox3a\Form\Model\FormModel;
use Ox3a\Form\Model;
use Zend\Filter;
use Zend\Validator;


class <?php echo $this->getShortClassName() ?> extends FormModel
{

    /**
     * @return void
     */
    public function init()
    {
<?php foreach ($this->getAttributes() as $attribute => $value) { ?>
        $this->setAttribute(<?php echo $printService->print($attribute) ?>, <?php echo $printService->print($value) ?>);
<?php } ?>

<?php foreach ($this->getElements() as $element) { ?>
        $this->add(
            <?php echo is_array($element)?trim($printService->printArray($element, 3)):$element ?>

        );
<?php } ?>

    }

    /**
     * @return <?php echo $this->getModelShortClassName() . PHP_EOL ?>
     */
    public function getDataModel()
    {
        $model = new <?php echo $this->getModelShortClassName() ?>();

        foreach ($this->getData() as $field => $value) {
            if (property_exists($model, $field)) {
                $model->$field = $value;
            }
        }

        return $model;
    }

}
