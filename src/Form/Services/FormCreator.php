<?php
/**
 * {Template_Description_Abstract}
 *
 * @author    Zakharov.A
 * @date      28.12.2020
 * @copyright {Template_Description_Copyrights}
 */

namespace Ox3a\CodeGenerators\Form\Services;

use Ox3a\Annotation\Analyzer;
use Ox3a\Annotation\Form;
use Ox3a\CodeGenerators\Form\Models\Elements;
use Ox3a\CodeGenerators\Models\ClassNameModel;
use Ox3a\CodeGenerators\Services\PrintService;
use Ox3a\Form\Model\ButtonModel;
use ReflectionClass;

class FormCreator
{
    /**
     * @var array|null
     */
    private $annotations;

    /**
     * Класс модели
     * @var ReflectionClass
     */
    private $class;

    private $typeNamespace;
    private $typeSuffix;

    private $formsNamespace = 'Forms';
    private $formSuffix = 'Form';

    /**
     * @var Elements\Factory
     */
    private $elementFactory;

    /**
     * @var PrintService
     */
    private $printService;

    /**
     * @return PrintService
     */
    public function getPrintService(): PrintService
    {
        if (!$this->printService) {
            $this->printService = new PrintService();
        }
        return $this->printService;
    }

    /**
     * @return Elements\Factory
     */
    public function getElementFactory(): Elements\Factory
    {
        if (!$this->elementFactory) {
            $this->elementFactory = new Elements\Factory();
        }
        return $this->elementFactory;
    }

    /**
     * @param mixed $typeNamespace
     */
    public function setTypeNamespace($typeNamespace): void
    {
        $this->typeNamespace = $typeNamespace;
    }

    /**
     * @param mixed $typeSuffix
     */
    public function setTypeSuffix($typeSuffix): void
    {
        $this->typeSuffix = $typeSuffix;
    }

    /**
     * @return array
     */
    public function getElements(): array
    {
        $elements    = [];
        $annotations = $this->getAnnotations();

        foreach ($annotations['properties'] as $property => $list) {
            if (isset($list[Form\Element::class])) {
                $elements[] = $this->clean($this->getElementFactory()->get($property, $list));
                if (($separator = $this->findSeparator($list))) {
                    $elements[] = $this->createSeparator($separator);
                }
            }
        }

        if (isset($annotations['class'][Form\SubmitButton::class])) {
            $submitParams = $annotations['class'][Form\SubmitButton::class][0];

            $submitParams['options']['label'] = $submitParams['label'] ?? '&nbsp;';
            $submitParams['options']['title'] = $submitParams['title'] ?? 'Сохранить';
            if ($submitParams['icon']) {
                $submitParams['options']['icon'] = $submitParams['icon'];
            }

            $elements[] = $this->clean([
                'type'       => new ClassNameModel(ButtonModel::class),
                'name'       => $submitParams['name'] ?? 'submit',
                'options'    => $submitParams['options'],
                'attributes' => $submitParams['attributes'],
            ]);
        } else {
            $elements[] = [
                'type'    => new ClassNameModel(ButtonModel::class),
                'name'    => 'submit',
                'options' => [
                    'label' => '&nbsp;',
                    'title' => 'Сохранить',
                ],
            ];
        }

        return $elements;
    }

    public function getAttributes()
    {
        $attributes  = [];
        $annotations = $this->getAnnotations();

        foreach ($annotations['class'] as $attribute => $list) {
            if ($attribute == Form\Attribute::class) {
                foreach ($list as $item) {
                    $attributes[$item['name']] = $item['value'];
                }
            }
        }

        return $attributes;
    }

    public function getFormsDir(string $entityPath)
    {
        return dirname(
            str_replace(
                str_replace('\\', '/', $this->typeNamespace),
                $this->formsNamespace,
                $entityPath
            )
        );
    }

    public function getNamespace()
    {
        return str_replace(
            $this->typeNamespace,
            $this->formsNamespace,
            $this->class->getNamespaceName()
        );
    }

    public function create($className)
    {
        $this->init($className);

        $code = $this->getCode();

        return [
            $this->getShortClassName(),
            $code,
        ];
    }

    public function getModelClassName()
    {
        return $this->class->getName();
    }

    public function getShortClassName()
    {
        return str_replace(
            $this->typeSuffix,
            $this->formSuffix,
            $this->getModelShortClassName()
        );
    }

    public function getModelShortClassName()
    {
        return $this->class->getShortName();
    }

    private function init($className)
    {
        $this->annotations = null;

        $this->class = new ReflectionClass($className);
    }

    private function getAnnotations()
    {
        if (is_null($this->annotations)) {
            $analyzer          = new Analyzer();
            $this->annotations = $analyzer->run($this->class->getName());
        }

        return $this->annotations;
    }

    private function getCode()
    {
        ob_start();

        require_once __DIR__ . '/templates/form.php';

        return ob_get_clean();
    }

    private function clean(array $element): array
    {
        foreach (['attributes', 'filters', 'validators', 'options'] as $key) {
            if (empty($element[$key])) {
                unset($element[$key]);
            }
        }
        return $element;
    }

    private function findSeparator(array $list): ?array
    {
        if (empty($list[Form\Attribute::class])) {
            return null;
        }

        foreach ($list[Form\Attribute::class] as $attr) {
            if ($attr['name'] == 'separator') {
                return $attr;
            }
        }
        return null;
    }

    private function createSeparator(array $separator): array
    {
        return $this->getElementFactory()->get($separator['value'] ?? 'separator', [
            Form\Element::class => [
                ['type' => 'separator'],
            ],
        ]);
    }

}
