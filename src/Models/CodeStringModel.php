<?php

namespace Ox3a\CodeGenerators\Models;

class CodeStringModel
{
    /**
     * @var string
     */
    private $code;

    /**
     * @param string $code
     */
    public function __construct(string $code)
    {
        $this->code = $code;
    }

    public function __toString()
    {
        return $this->code;
    }
}
