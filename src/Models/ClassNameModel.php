<?php
/**
 * {Template_Description_Abstract}
 *
 * @author    Zakharov.A
 * @date      11.02.2021
 * @copyright {Template_Description_Copyrights}
 */

namespace Ox3a\CodeGenerators\Models;


class ClassNameModel
{
    /**
     * @var string
     */
    private $value = '';


    /**
     * ClassNameModel constructor.
     * @param string $value
     */
    public function __construct(string $value)
    {
        $this->value = $value;
    }


    public function __toString()
    {
        return '\\' . $this->value . '::class';
    }


}
