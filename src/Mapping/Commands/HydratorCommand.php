<?php
/**
 * {Template_Description_Abstract}
 *
 * @author    Zakharov.A
 * @date      17.12.2020
 * @copyright {Template_Description_Copyrights}
 */

namespace Ox3a\CodeGenerators\Mapping\Commands;

use Ox3a\CodeGenerators\Mapping\Services\HydratorCreator;
use Ox3a\CodeGenerators\Services\ClassInfoService;
use Ox3a\CodeGenerators\Services\Settings\SettingsInterface;
use RuntimeException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class HydratorCommand extends Command
{

    protected static $defaultName = 'mapping:hydrator';

    /**
     * @var HydratorCreator
     */
    private $creator;

    /**
     * @param HydratorCreator $creator
     */
    public function __construct(HydratorCreator $creator)
    {
        $this->creator = $creator;

        parent::__construct();
    }

    protected function configure()
    {
        $this->addArgument('path', InputArgument::REQUIRED, 'Путь до модели');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $filePath     = $input->getArgument('path');
        $fullFilePath = realpath($filePath);
        $base         = mb_substr($fullFilePath, 0, -mb_strlen($filePath));

        require_once $fullFilePath;

        $creator = $this->creator;

        $result = $creator->create((new ClassInfoService())->getClassName($fullFilePath));

        $hydratorsDir = $creator->getHydratorsDir($fullFilePath);

        if (!is_dir($hydratorsDir)) {
            mkdir($hydratorsDir, 0777, true);
        }

        $outputPath = $hydratorsDir . '/' . $result[0] . '.php';
        if ($outputPath == $fullFilePath) {
            throw new RuntimeException('Попытка перезаписать исходник');
        }
        file_put_contents($outputPath, $result[1]);

        $output->writeln(sprintf("Создан гидратор: %s", mb_substr($outputPath, mb_strlen($base))));

        return self::SUCCESS;
    }
}
