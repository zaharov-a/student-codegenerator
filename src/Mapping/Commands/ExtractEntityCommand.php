<?php
/**
 * {Template_Description_Abstract}
 *
 * @author    Zakharov.A
 * @date      16.12.2020
 * @copyright {Template_Description_Copyrights}
 */

namespace Ox3a\CodeGenerators\Mapping\Commands;


use Ox3a\CodeGenerators\Mapping\EntityExtractor;
use Ox3a\Service\CaseService;
use Ox3a\Service\DbService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ExtractEntityCommand extends Command
{
    protected static $defaultName = 'mapping:extract-entity';


    protected function configure()
    {
        $this->addArgument('table', InputArgument::REQUIRED, 'Таблица для извлечения');
        $this->addArgument('dir', InputArgument::OPTIONAL, 'Папка для результата');
        $this->addOption('namespace', null, InputOption::VALUE_OPTIONAL, 'Namespace сущности');
        $this->addOption('class', null, InputOption::VALUE_OPTIONAL, 'Название класса сущности');
    }


    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $table = $input->getArgument('table');
        $dir   = $input->getArgument('dir');

        $entityExtractor = new EntityExtractor(DbService::getInstance(), new CaseService());

        $result    = $entityExtractor->extract($table, $input->getOption('namespace'), $input->getOption('class'));
        $className = $result[0];
        $content   = $result[1];
        if ($dir) {
            file_put_contents("{$dir}/{$className}.php", $content);
        } else {
            $output->write($content);
        }

        return self::SUCCESS;
    }

}
