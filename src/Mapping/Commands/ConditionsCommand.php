<?php
/**
 * {Template_Description_Abstract}
 *
 * @author    Zakharov.A
 * @date      17.12.2020
 * @copyright {Template_Description_Copyrights}
 */

namespace Ox3a\CodeGenerators\Mapping\Commands;

use Ox3a\CodeGenerators\Mapping\Services\ConditionsCreator;
use Ox3a\CodeGenerators\Services\ClassInfoService;
use Ox3a\CodeGenerators\Services\Settings\SettingsInterface;
use RuntimeException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ConditionsCommand extends Command
{

    protected static $defaultName = 'mapping:conditions';

    /**
     * @var ConditionsCreator
     */
    private $creator;

    /**
     * @param ConditionsCreator $creator
     */
    public function __construct(ConditionsCreator $creator)
    {
        $this->creator = $creator;

        parent::__construct();
    }

    protected function configure()
    {
        $this->addArgument('path', InputArgument::REQUIRED, 'Путь до модели');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $filePath     = $input->getArgument('path');
        $fullFilePath = realpath($filePath);
        $base         = mb_substr($fullFilePath, 0, -mb_strlen($filePath));

        require_once $fullFilePath;

        $creator = $this->creator;

        $result = $creator->create((new ClassInfoService())->getClassName($filePath));

        $conditionsDir = $creator->getConditionsDir($fullFilePath);

        if (!is_dir($conditionsDir)) {
            mkdir($conditionsDir, 0777, true);
        }

        $outputPath = $conditionsDir . '/' . $result[0] . '.php';

        if ($outputPath == $fullFilePath) {
            throw new RuntimeException('Попытка перезаписать исходник');
        }
        file_put_contents($outputPath, $result[1]);

        $output->writeln(sprintf("Создан сборщик условий: %s", mb_substr($outputPath, mb_strlen($base))));

        return self::SUCCESS;
    }
}
