<?php
/**
 * {Template_Description_Abstract}
 *
 * @author    Zakharov.A
 * @date      24.10.2021
 * @copyright {Template_Description_Copyrights}
 */

namespace Ox3a\CodeGenerators\Mapping\Commands;

use RuntimeException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class FullCommand extends Command
{
    protected static $defaultName = "mapping:full";

    protected function configure()
    {
        $this->addArgument('path', InputArgument::REQUIRED, 'Путь до модели');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $path = $input->getArgument('path');
        $commands = [
            'mapping:conditions',
            'mapping:hydrator',
            'mapping:mapper',
        ];

        foreach ($commands as $commandName) {
            $command = $this->getApplication()->find($commandName);

            $commandInput = new ArrayInput(
                [
                    'path' => $path,
                ]
            );

            if ($command->run($commandInput, $output)) {
                throw new RuntimeException("Что-то пошло не так при выполнении: {$commandName}");
            }
        }

        return self::SUCCESS;
    }

}
