<?php

namespace Ox3a\CodeGenerators\Mapping\Services\Builders;

use Nette\PhpGenerator\ClassType;
use Nette\PhpGenerator\PhpFile;
use Nette\PhpGenerator\PhpNamespace;
use Ox3a\Annotation\Analyzer;
use Ox3a\Annotation\Mapping\Expression;
use ReflectionClass;
use Amd\AmdCoreServices\ConditionsBuilder\ConditionsBuilder;
use Amd\AmdCoreServices\Db\DbInterface;

class AMDMapperBuilder implements MapperBuilderInterface
{

    /**
     * @var array|array[]
     */
    private $annotations;

    /**
     * @var ReflectionClass
     */
    private $class;

    private $className;
    /**
     * @var array
     */
    private $mapper;
    /**
     * @var array
     */
    private $entity;
    /**
     * @var array
     */
    private $hydrator;
    /**
     * @var array
     */
    private $conditions;

    public function build(array $mapper, array $entity, array $hydrator, array $conditions): PhpFile
    {
        $this->className = $entity['class'];

        $this->mapper     = $mapper;
        $this->entity     = $entity;
        $this->hydrator   = $hydrator;
        $this->conditions = $conditions;

        $file = new PhpFile();

        $file->addComment("auto generated");
        $namespaceObject = new PhpNamespace($namespace = $mapper['namespace']);

        $class = $namespaceObject->addClass($shortName = $mapper['shortName']);

        $class->addComment("Class {$shortName}")
            ->addComment("@package {$namespace}");

        $file->addNamespace($namespaceObject);

        $this
            ->addUses($namespaceObject)
            ->addProperties($class)
            ->addConstructor($class)
            ->addSaveMethod($class)
            ->addFindByMethod($class)
            ->addDeleteMethod($class)
            ->addGetSelectMethod($class)
            ->addCreateEntityMethod($class)
            ->addProtectedMethods($class);

        return $file;
    }

    private function getClass(): ReflectionClass
    {
        return $this->class ?: ($this->class = new ReflectionClass($this->className));
    }

    private function getAnnotations(): array
    {
        if (!$this->annotations) {
            $analyzer          = new Analyzer();
            $this->annotations = $analyzer->run($this->class->getName());
        }
        return $this->annotations;
    }

    private function addUses(PhpNamespace $namespaceObject)
    {
        $namespaceObject->addUse($this->entity['class'])
            ->addUse($this->hydrator['class'])
            ->addUse($this->conditions['class'])
            ->addUse(DbInterface::class)
            ->addUse(\Zend_Db_Select::class)
            ->addUse(\Zend_Db_Expr::class)
            ->addUse(ConditionsBuilder::class)
            ->addUse(\Zend_Db_Table_Abstract::class);

        if (!empty($this->entity['concrete'])) {
            $namespaceObject->addUse($this->entity['concrete']['class']);
        }

        return $this;
    }

    private function addProperties(ClassType $class)
    {
        $class->addProperty('table')
//            ->addComment('Название таблицы')
            ->addComment('@var string')
            ->setValue($this->entity['table']['name'])
            ->setPrivate();

        $class->addProperty('primaryKey')
//            ->addComment('Название таблицы')
            ->addComment('@var string')
            ->setValue($this->entity['primaryKey'])
            ->setPrivate();

        $class->addProperty('entityClass')
            ->addComment('Класс сущности')
            ->addComment('@var string')
            ->setValue(
                ($this->entity['concrete'] ? $this->entity['concrete']['shortName'] : $this->entity['shortName']) . '::class'
            )
            ->setPrivate();

        $class->addProperty('dbService')
//            ->addComment('Название таблицы')
            ->addComment('@var DbInterface')
            ->setPrivate();

        $class->addProperty('conditionsBuilder')
//            ->addComment('Название таблицы')
            ->addComment('@var ConditionsBuilder')
            ->setPrivate();

        return $this;
    }

    private function addConstructor(ClassType $class)
    {
        $method = $class->addMethod('__construct')
            ->addComment('@param DbInterface       $dbService')
            ->addComment('@param ConditionsBuilder $conditionsBuilder')
            ->addBody('$this->dbService         = $dbService;')
            ->addBody('$this->conditionsBuilder = $conditionsBuilder;');

        $method->addParameter('dbService')->setType(DbInterface::class);
        $method->addParameter('conditionsBuilder')->setType(ConditionsBuilder::class);

        return $this;
    }

    private function addSaveMethod(ClassType $class)
    {
        $method = $class->addMethod('save')
            ->addComment('Сохранить')
            ->addComment('@param ' . $this->entity['shortName'] . ' $entity');

        $method->addParameter('entity')
            ->setType($this->entity['class']);

        $method->addBody('$hydrator = $this->getHydrator();');
        $method->addBody('');
        $method->addBody('$masterData = $hydrator->extract($entity);');
        $method->addBody('$primaryId  = $masterData[$this->primaryKey];');
        $method->addBody('');

        if ($this->entity['isDeletable']) {
            $method->addBody(
                <<<'CODE'
if ($primaryId) {
    if ($entity->isDelete()) {
        $this->delete($primaryId);
    } else {
        $this->dbService->update($this->table, $masterData, ["{$this->primaryKey}=?" => $primaryId]);
    }
} else {
    if (!$entity->isDelete()) {
        $primaryId = $this->dbService->insert($this->table, $masterData);
        $masterData[$this->primaryKey] = $primaryId;
        $hydrator->hydrate($entity, [$this->primaryKey => $primaryId]);
    }
}
CODE
            );
        } else {
            $method->addBody(
                <<<'CODE'
if ($primaryId) {
    $this->dbService->update($this->table, $masterData, ["{$this->primaryKey}=?" => $primaryId]);
} else {
    $primaryId = $this->dbService->insert($this->table, $masterData);
    $masterData[$this->primaryKey] = $primaryId;
    $hydrator->hydrate($entity, [$this->primaryKey => $primaryId]);
}
CODE
            );
        }

        return $this;
    }

    private function addFindByMethod(ClassType $class)
    {
        $concrete = $this->entity['concrete'] ?: $this->entity;

        $method = $class->addMethod('findBy')
            ->addComment('Найти сущности по условиям')
            ->addComment('@param ' . $this->conditions['shortName'] . ' $conditions')
            ->addComment('@return ' . $concrete['shortName'] . '[]')
            ->setReturnType('array');

        $method->addParameter('conditions')
            ->setType($this->conditions['class']);

        $method->setBody(
            <<<'CODE'
$select = $this->getSelect();

if (($condition = $this->conditionsBuilder->build($conditions->getConditions()))) {
    $select->where($condition);
}

if (($fields = $conditions->getOrder())) {
    $order = [];
    foreach($fields as $field => $direction){
        $order[] = "{$field} {$direction}";
    }
    $select->order($order);
}

if ($conditions->getLimit()) {
    $select->limit($conditions->getLimit(), $conditions->getOffset());
}

return array_map(function ($row) {
    return $this->createEntity($row);
}, $select->query()->fetchAll(DbInterface::FETCH_ASSOC));
CODE
        );

        return $this;
    }

    private function addDeleteMethod(ClassType $class)
    {
        $class->addMethod('delete')
            ->addComment('Удалить')
            ->addComment('@param int $id')
            ->addBody('$this->dbService->delete($this->table, [$this->primaryKey . \'=?\' => $id]);')
            ->addParameter('id')
            ->setType('int');

        return $this;
    }

    private function addGetSelectMethod(ClassType $class)
    {
        $table = $this->entity['table'];
        if ($table['alias']) {
            $tableWithAlias = sprintf("['%s' => \$this->table]", $table['alias']);
        } else {
            $tableWithAlias = "\$this->table";
        }

        $body = '$select = new Zend_Db_Select($this->dbService->getRealAdapter());

return $select
    ->from(
        ' . $tableWithAlias . ',
        ' . $this->getMainColumns() . '
    )' . $this->getJoins() . ';
';

        $class->addMethod('getSelect')
            ->addComment('Получить селект для выборки')
            ->addComment('@return Zend_Db_Select')
            ->setReturnType('Zend_Db_Select')
            ->setBody($body);

        return $this;
    }

    private function addCreateEntityMethod(ClassType $class)
    {
        $entityClass = $this->entity['shortName'];
        if ($this->entity['concrete']) {
            $entityClass = $this->entity['concrete']['shortName'] . "|{$entityClass}";
        }

        $class->addMethod('createEntity')
            ->addComment('Создать сущность')
            ->addComment('@param array $data')
            ->addComment('@return ' . $entityClass)
            ->addBody('$entityClass = $this->entityClass;')
            ->addBody('$entity = new $entityClass();')
            ->addBody('')
            ->addBody('$this->getHydrator()->hydrate($entity, $data);')
            ->addBody('')
            ->addBody('return $entity;')
            ->addParameter('data')
            ->setDefaultValue([]);

        return $this;
    }

    private function addProtectedMethods(ClassType $class)
    {
        $class
            ->addMethod('getPrimaryKey')
            ->addComment('Получить имя поля первичного ключа')
            ->addComment('@return string')
            ->addBody('return $this->primaryKey;')
            ->setReturnType('string')
            ->setProtected();

        $class
            ->addMethod('getHydrator')
            ->addComment('Получить гидратор')
            ->addComment('@return ' . $this->hydrator['shortName'])
            ->addBody('return new ' . $this->hydrator['shortName'] . '();')
            ->setReturnType($this->hydrator['class'])
            ->setProtected();

        return $this;
    }

    private function getMainColumns()
    {
        $columns = $this->entity['table']['columns'];

        return $columns
            ? sprintf(
                "[\n            %s,\n        ]",
                implode(
                    ",\n            ",
                    $this->getSelectColumns($columns)
                )
            )
            : '[]';
    }

    private function getJoins()
    {
        $joins     = $this->entity['joins'];
        $leftJoins = $this->entity['leftJoins'];

        if (!($joins || $leftJoins)) {
            return '';
        }

        $result = [];

        foreach ($joins as $join) {
            $result[] = $this->joinToString($join['columns'], $join);
        }

        foreach ($leftJoins as $join) {
            $result[] = $this->joinToString($join['columns'], $join, 'left');
        }

        return $result ? (implode("\n    ->", array_merge([''], $result))) : '';
    }

    private function joinToString(array $cols, array $join, $type = 'inner'): string
    {
        if ($join['alias']) {
            $table = "['{$join['alias']}' => '{$join['table']}']";
        } else {
            $table = $join['table'];
        }

        // $cols = $this->getTableColumns($join['alias'] ?: $join['table']);

        $cols = $cols
            ? sprintf(
                "[\n            %s,\n        ]",
                implode(
                    ",\n            ",
                    $this->getSelectColumns($cols)
                )
            )
            : '[]';

        $method = $type == 'inner' ? 'join' : 'joinLeft';
        return sprintf(
            "{$method}(\n        %s,\n        '%s',\n        %s\n    )",
            $table,
            $join['on'],
            $cols
        );
    }

    private function getSelectColumns($columns)
    {
        return array_map(function ($col) {
            if (is_array($col)) {
                return "'{$col[0]}' => new Zend_Db_Expr(\"$col[1]\")";
            }
            return "'{$col}'";
        }, $columns);
    }
}
