<?php

namespace Ox3a\CodeGenerators\Mapping\Services\Builders;

use Nette\PhpGenerator\ClassType;
use Nette\PhpGenerator\PhpFile;
use Nette\PhpGenerator\PhpNamespace;
use Ox3a\Annotation\Analyzer;
use Ox3a\Service\DbServiceInterface;
use ReflectionClass;

class PSKMapperBuilder implements MapperBuilderInterface
{

    /**
     * @var array|array[]
     */
    private $annotations;

    /**
     * @var ReflectionClass
     */
    private $class;

    private $className;
    /**
     * @var array
     */
    private $mapper;
    /**
     * @var array
     */
    private $entity;
    /**
     * @var array
     */
    private $hydrator;
    /**
     * @var array
     */
    private $conditions;

    public function build(array $mapper, array $entity, array $hydrator, array $conditions): PhpFile
    {
        $this->className = $entity['class'];

        $this->mapper     = $mapper;
        $this->entity     = $entity;
        $this->hydrator   = $hydrator;
        $this->conditions = $conditions;

        $file = new PhpFile();

        $file->addComment("auto generated");
        $namespaceObject = new PhpNamespace($namespace = $mapper['namespace']);

        $class = $namespaceObject->addClass($shortName = $mapper['shortName']);

        $class->addComment("Class {$shortName}")
            ->addComment("@package {$namespace}");

        $file->addNamespace($namespaceObject);

        $this->addUses($namespaceObject);
        $this->addProperties($class);
        $this->addConstructor($class);

        if (!$mapper['isViewonly']) {
            $this->addSaveMethod($class);
            $this->addAddSetFunctions($class);
        }
        $this->addFindByMethod($class);
        if (!$mapper['isViewonly']) {
            $this->addDeleteMethod($class);
        }
        $this->addGetSelectMethod($class);
        $this->addCreateEntityMethod($class);
        $this->addProtectedMethods($class);

        return $file;
    }

    private function getClass(): ReflectionClass
    {
        return $this->class ?: ($this->class = new ReflectionClass($this->className));
    }

    private function getAnnotations(): array
    {
        if (!$this->annotations) {
            $analyzer          = new Analyzer();
            $this->annotations = $analyzer->run($this->class->getName());
        }
        return $this->annotations;
    }

    private function addUses(PhpNamespace $namespaceObject)
    {
        $namespaceObject->addUse($this->entity['class'])
            ->addUse($this->hydrator['class'])
            ->addUse($this->conditions['class'])
            ->addUse(DbServiceInterface::class)
            ->addUse(\Zend\Db\Sql\Select::class)
            ->addUse(\Zend\Db\Sql\Predicate\Expression::class)
            ->addUse(\Ox3a\Core\ConditionsBuilder\ConditionsBuilder::class)
            ->addUse(\Zend\Db\TableGateway\TableGateway::class)
            ->addUse(\Zend\Db\Sql\Platform\Platform::class)
            ->addUse(\ReflectionException::class);

        if (!empty($this->entity['concrete'])) {
            $namespaceObject->addUse($this->entity['concrete']['class']);
        }

        return $this;
    }

    private function addProperties(ClassType $class)
    {
        $class->addProperty('table')
            ->addComment('@var non-empty-string')
            ->setValue($this->entity['table']['name'])
            ->setPrivate();

        $class->addProperty('primaryKey')
            ->addComment('@var non-empty-string')
            ->setValue($this->entity['primaryKey'])
            ->setPrivate();

        $class->addProperty('dbService')
            ->addComment('@var DbServiceInterface')
            ->setPrivate();

        $class->addProperty('conditionsBuilder')
            ->addComment('@var ConditionsBuilder')
            ->setPrivate();

        return $this;
    }

    private function addConstructor(ClassType $class)
    {
        $method = $class->addMethod('__construct')
            ->addComment('@param DbServiceInterface $dbService')
            ->addComment('@param ConditionsBuilder  $conditionsBuilder')
            ->addBody('$this->dbService         = $dbService;')
            ->addBody('$this->conditionsBuilder = $conditionsBuilder;');

        $method->addParameter('dbService')->setType(DbServiceInterface::class);
        $method->addParameter('conditionsBuilder')->setType(\Ox3a\Core\ConditionsBuilder\ConditionsBuilder::class);

        return $this;
    }

    private function addSaveMethod(ClassType $class)
    {
        $method = $class->addMethod('save')
            ->addComment('Сохранить')
            ->addComment('@param ' . $this->entity['shortName'] . ' $entity')
            ->addComment('@return void')
            ->addComment('@throws ReflectionException');

        $method->addParameter('entity')
            ->setType($this->entity['class']);

        $method->addBody('$hydrator = $this->getHydrator();');
        if ($this->hasSetters()) {
            $method->addBody('$data     = $this->addSetFunctions($hydrator->extract($entity));');
        } else {
            $method->addBody('$data     = $hydrator->extract($entity);');
        }
        $method->addBody('');
        $method->addBody('$primaryId = $data[$this->primaryKey];');
        $method->addBody('');

        if ($this->entity['isDeletable']) {
            $method->addBody(
                <<<'CODE'
if ($primaryId) {
    if ($entity->isDelete()) {
        $this->delete($primaryId);
    } else {
        $this->getTable()->update($data, ["{$this->primaryKey}=?" => $primaryId]);
    }
} else {
    if (!$entity->isDelete()) {
        $hydrator->hydrate($entity, [$this->primaryKey => $this->getTable()->insert($data)]);
    }
}
CODE
            );
        } else {
            $method->addBody(
                <<<'CODE'
if ($primaryId) {
    $this->getTable()->update($data, ["{$this->primaryKey}=?" => $primaryId]);
} else {
    $this->getTable()->insert($data);
    $primaryId = $this->getTable()->getLastInsertValue();
    $hydrator->hydrate($entity, [$this->primaryKey => $primaryId]);
}
CODE
            );
        }

        return $this;
    }

    private function addAddSetFunctions(ClassType $class)
    {
        if (!$this->hasSetters()) {
            return $this;
        }
        $columns = $this->entity['table']['columns'];
        $body    = '';
        foreach ($columns as $column) {
            if (empty($column['set'])) {
                continue;
            }
            $field = $column['name'];
            $body  .= "\$data['{$field}'] = new Expression(sprintf(\"{$column['set']}\", \$this->dbService->quote(\$data['{$field}'])));\n";
        }
        $body   .= 'return $data;';
        $method = $class->addMethod('addSetFunctions')
            ->setPrivate()
            ->addComment('Добавить функции для запроса обновления')
            ->addComment('@param array $data')
            ->addComment('@return array')
            ->setBody($body);
        $method->addParameter('data')->setType('array');
        return $this;
    }

    private function hasSetters()
    {
        $columns = $this->entity['table']['columns'];
        foreach ($columns as $column) {
            if (empty($column['set'])) {
                continue;
            }
            return true;
        }
        return false;
    }

    private function addFindByMethod(ClassType $class)
    {
        $method = $class->addMethod('findBy')
            ->addComment('Найти сущности по условиям')
            ->addComment('@param ' . $this->conditions['shortName'] . ' $conditions')
            ->addComment('@return list<' . $this->entity['shortName'] . '>');

        $method->addParameter('conditions')
            ->setType($this->conditions['class']);

        $method->setBody(
            <<<'CODE'
$select = $this->getSelect();

$condition = $this->conditionsBuilder->build($conditions->getConditions());
if ($condition->count()) {
    $select->where($condition);
}

if (($order = $conditions->getOrder())) {
    $select->order($order);
} 

if ($conditions->getLimit()) {
    $select->limit($conditions->getLimit());
    if ($conditions->getOffset()) {
        $select->offset($conditions->getOffset());
    }
}

/** @var list<array<non-empty-string, mixed>> $list */
$list = $this->dbService->fetchAll($this->buildSql($select));
return array_map(function ($row) {
    return $this->createEntity($row);
}, $list);
CODE
        );

        return $this;
    }

    private function addDeleteMethod(ClassType $class)
    {
        $class->addMethod('delete')
            ->addComment('Удалить')
            ->addComment('@param int $id')
            ->addComment('@return void')
            ->addBody('$this->getTable()->delete([$this->primaryKey . \'=?\' => $id]);')
            ->addParameter('id');

        return $this;
    }

    private function addGetSelectMethod(ClassType $class)
    {
        $table = $this->entity['table'];
        if ($table['alias']) {
            $tableWithAlias = sprintf("['%s' => \$this->table]", $table['alias']);
        } else {
            $tableWithAlias = "\$this->table";
        }

        $body = '$select = new Select();

return $select
    ->from(
        ' . $tableWithAlias . '
    )
    ->columns(
        ' . $this->getMainColumns() . '
    )' . $this->getJoins() . ';
';

        $class->addMethod('getSelect')
            ->addComment('Получить селект для выборки')
            ->addComment('@return Select')
            ->setBody($body);

        return $this;
    }

    private function addCreateEntityMethod(ClassType $class)
    {
        $entityClass = $this->entity['shortName'];
        if ($this->entity['concrete']) {
            $entityClass = $this->entity['concrete']['shortName'] . "|{$entityClass}";
        }

        $class->addMethod('createEntity')
            ->addComment('Создать сущность')
            ->addComment('@param array<non-empty-string, mixed> $data')
            ->addComment('@return ' . $entityClass)
            ->addBody("\$entity = new $entityClass();")
            ->addBody('')
            ->addBody('$this->getHydrator()->hydrate($entity, $data);')
            ->addBody('')
            ->addBody('return $entity;')
            ->addParameter('data')
            ->setDefaultValue([]);

        return $this;
    }

    private function addProtectedMethods(ClassType $class)
    {
        $class
            ->addMethod('getTable')
            ->addComment('Получить таблицу')
            ->addComment('@return TableGateway')
            ->addBody('return $this->dbService->getTable($this->table);')
            ->setProtected();

        $class
            ->addMethod('getPrimaryKey')
            ->addComment('Получить имя поля первичного ключа')
            ->addComment('@return string')
            ->addBody('return $this->primaryKey;')
            ->setProtected();

        $class
            ->addMethod('getHydrator')
            ->addComment('Получить гидратор')
            ->addComment('@return ' . $this->hydrator['shortName'])
            ->addBody('return new ' . $this->hydrator['shortName'] . '();')
            ->setProtected();

        $class
            ->addMethod('buildSql')
            ->addComment('Собрать SQL')
            ->addComment('@param Select $select')
            ->addComment('@return string')
            ->addBody('$platform = new Platform($this->dbService->getAdapter());')
            ->addBody('$platform->setSubject($select);')
            ->addBody('return $platform->getSqlString();')
            ->setProtected()
            ->addParameter('select')->setType(\Zend\Db\Sql\Select::class);

        return $this;
    }

    private function getMainColumns()
    {
        $columns = $this->entity['table']['columns'];

        return $columns
            ? sprintf(
                "[\n            %s,\n        ]",
                implode(
                    ",\n            ",
                    $this->columnsToStrings($columns)
                )
            )
            : '[]';
    }

    private function getJoins()
    {
        $joins     = $this->entity['joins'];
        $leftJoins = $this->entity['leftJoins'];

        if (!($joins || $leftJoins)) {
            return '';
        }

        $result = [];

        foreach ($joins as $join) {
            $result[] = $this->joinToString($join['columns'], $join);
        }

        foreach ($leftJoins as $join) {
            $result[] = $this->joinToString($join['columns'], $join, 'left');
        }

        return $result ? (implode("\n    ->", array_merge([''], $result))) : '';
    }

    private function joinToString(array $cols, array $join, $type = 'inner'): string
    {
        if ($join['alias']) {
            $table = "['{$join['alias']}' => '{$join['table']}']";
        } else {
            $table = $join['table'];
        }

        $cols = $cols
            ? sprintf(
                "[\n            %s,\n        ]",
                implode(
                    ",\n            ",
                    $this->columnsToStrings($cols)
                )
            )
            : '[]';

        if (is_array($join['on'])) {
            $on = sprintf('new Expression("%s", ["%s"])', $join['on'][0], implode('", "', array_slice($join['on'], 1)));
        } else {
            $on = "'{$join['on']}'";
        }
        $typeConstant = $type == 'inner' ? 'Select::JOIN_INNER' : 'Select::JOIN_LEFT';
        return sprintf(
            "join(\n        %s,\n        %s,\n        %s,\n        %s\n    )",
            $table,
            $on,
            $cols,
            $typeConstant
        );
    }

    private function columnsToStrings(array $columns): array
    {
        return array_map(function ($col) {
            if ($col['get']) {
                return "'{$col['alias']}' => new Expression(\"{$col['get']}\")";
            }

            return "'{$col['alias']}' => \"{$col['name']}\"";
        }, $columns);
    }
}
