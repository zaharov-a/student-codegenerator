<?php
/**
 * {Template_Description_Abstract}
 *
 * @author    Zakharov.A
 * @date      03.12.2021
 * @copyright {Template_Description_Copyrights}
 */

namespace Ox3a\CodeGenerators\Mapping\Services\Builders;

use Nette\PhpGenerator\ClassType;
use Nette\PhpGenerator\PhpFile;
use Nette\PhpGenerator\PhpNamespace;

class HydratorBuilder
{

    /**
     * Список используемых типов в гидраторе
     * @var array
     */
    private $usedTypes = [];

    public function build($properties, $readonly, $hydrator, $entity)
    {
        $this->calculateUserTypes($properties);

        $namespace       = $hydrator['namespace'];
        $shortName       = $hydrator['shortName'];
        $namespaceObject = new PhpNamespace($namespace);

        $file = new PhpFile();

        $file->addComment("auto generated");

        $class = $namespaceObject->addClass($shortName);

        $class->addComment("Class {$shortName}")
            ->addComment("@package {$namespace}");

        $file->addNamespace($namespaceObject);

        $this->addUses($namespaceObject, $entity['class'])
            ->addProperties($class, $properties, $readonly, $entity)
            ->addBaseMethods($class, $readonly, $entity, $hydrator)
            ->addHydrateExtractMethods($class, $hydrator);

        return $file;
    }

    public function isTypeUsed($type): bool
    {
        return in_array($type, $this->usedTypes);
    }

    private function addUses(PhpNamespace $namespaceObject, string $entityClass)
    {
        $namespaceObject
            ->addUse('ReflectionClass')
            ->addUse('ReflectionProperty')
            ->addUse('ReflectionException')
            ->addUse($entityClass);
        if ($this->isTypeUsed('Date') || $this->isTypeUsed('DateTime')) {
            $namespaceObject
                ->addUse('DateTimeImmutable')
                ->addUse('DateTimeInterface');
        }

        return $this;
    }

    private function addProperties(ClassType $class, $properties, $readonly, $entity)
    {
        $class->addProperty('propertyReflections')
            ->setPrivate()
            ->setStatic()
            ->addComment('@var array<non-empty-string, ReflectionProperty>')
            ->setValue([]);

        $class->addProperty('classReflection')
            ->setPrivate()
            ->setStatic()
            ->addComment("@var ReflectionClass<{$entity['shortName']}>|null");

        $class->addProperty('map')
            ->setPrivate()
            ->addComment('Карта полей')
            ->addComment('@var array<non-empty-string, array{non-empty-string, non-empty-string}>')
            ->setValue($properties);

        if ($readonly) {
            $class->addProperty('readonly')
                ->setPrivate()
                ->addComment('Список полей для исключения из записи')
                ->addComment('@var string[]')
                ->setValue($readonly);
        }

        return $this;
    }

    private function addBaseMethods(ClassType $class, $readonly, array $entity, array $hydrator)
    {
        $class->addMethod('getReflectionProperty')
            ->setPrivate()
            ->setStatic()
            ->addComment('@param non-empty-string $property')
            ->addComment('@return ReflectionProperty')
            ->addComment('@throws ReflectionException')
            ->addBody(
                <<<'CODE'
if (!isset(self::$propertyReflections[$property])) {
    $reflectionClass = self::getReflectionClass();

    $reflectionProperty = $reflectionClass->getProperty($property);
    $reflectionProperty->setAccessible(true);

    self::$propertyReflections[$property] = $reflectionProperty;
}

return self::$propertyReflections[$property];
CODE
            )
            ->addParameter('property');

        $class->addMethod('getReflectionClass')
            ->setPrivate()
            ->setStatic()
            ->addComment("@return ReflectionClass<{$entity['shortName']}>")
            ->addBody(
                "return self::\$classReflection ?: self::\$classReflection = new ReflectionClass({$entity['shortName']}::class);"
            );

        $hydrateMethod = $class->addMethod('hydrate')
            ->addComment('Заполнить объект данными')
            ->addComment(sprintf('@param %s $object', $entity['shortName']))
            ->addComment('@param array<non-empty-string, mixed> $data')
            ->addComment(sprintf('@return %s', $entity['shortName']))
            ->addBody('foreach ($this->map as $property => $settings) {')
            ->addBody('    if (array_key_exists($property, $data)) {')
            ->addBody('        $value         = $data[$property];')
            ->addBody('        $hydrateMethod = "create" . ucfirst($settings[1]);')
            ->addBody('')
            ->addBody('        $this->hydrateProperty($object, $property, $this->{$hydrateMethod}($value));');

        $hydrateMethod->addBody('    }');
        $hydrateMethod->addBody('}');
        $hydrateMethod->addBody('');
        $hydrateMethod->addBody('return $object;');

        $hydrateMethod->addParameter('object')
            ->setType($entity['class']);

        $hydrateMethod->addParameter('data')
            ->setType('array');

        if (!$hydrator['isViewonly']) {
            $extractMethod = $class->addMethod('extract')
                ->addComment('Извлечь данные из объекта')
                ->addComment(sprintf('@param %s $object', $entity['shortName']))
                ->addComment('@return array<non-empty-string, mixed>')
                ->addComment('@throws ReflectionException')
                ->addBody('$dbData = [];')
                ->addBody('')
                ->addBody('foreach ($this->map as $property => $settings) {');
            if ($readonly) {
                $extractMethod
                    ->addBody('    if (in_array($property, $this->readonly)) {')
                    ->addBody('        continue;')
                    ->addBody('    }');
            }

            $extractMethod
                ->addBody('    $field         = $settings[0];')
                ->addBody('    $extractMethod = "extract" . ucfirst($settings[1]);')
                ->addBody('')
                ->addBody('    $dbData[$field] = $this->{$extractMethod}($this->extractProperty($object, $property));')
                ->addBody('}')
                ->addBody('')
                ->addBody('return $dbData;');

            $extractMethod->addParameter('object')
                ->setType($entity['class']);
        }

        $hydratePropertyMethod = $class->addMethod('hydrateProperty')
            ->setPublic()
            ->addComment('Заполнить данными свойство объекта')
            ->addComment(sprintf('@param %s $object', $entity['shortName']))
            ->addComment('@param non-empty-string $property')
            ->addComment('@param mixed $value')
            ->addComment('@return void')
            ->addComment('@throws ReflectionException')
            ->addBody('self::getReflectionProperty($property)->setValue($object, $value);');

        $hydratePropertyMethod->addParameter('object')
            ->setType($entity['class']);
        $hydratePropertyMethod->addParameter('property')// ->setType('string')
        ;
        $hydratePropertyMethod->addParameter('value')// ->setType('string')
        ;

        if (!$hydrator['isViewonly']) {
            $extractPropertyMethod = $class->addMethod('extractProperty')
                ->setPublic()
                ->addComment('Извлечь данные из свойства объекта')
                ->addComment(sprintf('@param %s $object', $entity['shortName']))
                ->addComment('@param non-empty-string $property')
                ->addComment('@return mixed')
                ->addComment('@throws ReflectionException')
                ->addBody('return self::getReflectionProperty($property)->getValue($object);');

            $extractPropertyMethod->addParameter('object')
                ->setType($entity['class']);
            $extractPropertyMethod->addParameter('property')// ->setType('string')
            ;
        }
        return $this;
    }

    private function addHydrateExtractMethods(ClassType $class, $hydrator)
    {
        $types = ['string', 'int', 'float', 'bool', 'json', 'Date', 'DateTime'];

        foreach ($types as $type) {
            if (!$this->isTypeUsed($type)) {
                continue;
            }

            $method = sprintf('add%sHydrate', ucfirst($type));

            $this->{$method}($class);

            if (!$hydrator['isViewonly']) {
                $method = sprintf('add%sExtract', ucfirst($type));

                $this->{$method}($class);
            }
        }
        return $this;
    }

    private function addStringHydrate(ClassType $class)
    {
        $class
            ->addMethod('createString')
            ->addComment('Создать строковое значение')
            ->addComment('@param string|null $value')
            ->addComment('@return string|null')
            ->setBody('return is_null($value) ? null : ((string)$value);')
            ->setPrivate()
            ->addParameter('value');
    }

    private function addStringExtract(ClassType $class)
    {
        $class
            ->addMethod('extractString')
            ->addComment('Извлечь строковое значение')
            ->addComment('@param string|null $value')
            ->addComment('@return string|null')
            ->setBody('return $value;')
            ->setPrivate()
            ->addParameter('value');
    }

    private function addIntHydrate(ClassType $class)
    {
        $class
            ->addMethod('createInt')
            ->addComment('Создать целочисленное значение')
            ->addComment('@param string|null $value')
            ->addComment('@return int|null')
            ->setBody('return is_null($value) ? null : ((int)$value);')
            ->setPrivate()
            ->addParameter('value');
    }

    private function addIntExtract(ClassType $class)
    {
        $class
            ->addMethod('extractInt')
            ->addComment('Извлечь целочисленное значение')
            ->addComment('@param int|null $value')
            ->addComment('@return int|null')
            ->setBody('return $value;')
            ->setPrivate()
            ->addParameter('value');
    }

    private function addFloatHydrate(ClassType $class)
    {
        $class
            ->addMethod('createFloat')
            ->addComment('Создать дробное значение')
            ->addComment('@param string|null $value')
            ->addComment('@return float|null')
            ->setBody('return is_null($value) ? null : ((float)$value);')
            ->setPrivate()
            ->addParameter('value');
    }

    private function addFloatExtract(ClassType $class)
    {
        $class
            ->addMethod('extractFloat')
            ->addComment('Извлечь дробное значение')
            ->addComment('@param float|null $value')
            ->addComment('@return float|null')
            ->setBody('return $value;')
            ->setPrivate()
            ->addParameter('value');
    }

    private function addBoolHydrate(ClassType $class)
    {
        $class
            ->addMethod('createBool')
            ->addComment('Создать логическое значение')
            ->addComment('@param string|null $value')
            ->addComment('@return bool|null')
            ->setBody('return is_null($value) ? null : ((bool)$value);')
            ->setPrivate()
            ->addParameter('value');
    }

    private function addBoolExtract(ClassType $class)
    {
        $class
            ->addMethod('extractBool')
            ->addComment('Извлечь логическое значение')
            ->addComment('@param bool|null $value')
            ->addComment('@return int|null')
            ->setBody('return is_null($value) ? null : ((int)(bool)$value);')
            ->setPrivate()
            ->addParameter('value');
    }

    private function addJsonHydrate(ClassType $class)
    {
        $class
            ->addMethod('createJson')
            ->addComment('string => json')
            ->addComment('@param string|null $value')
            ->addComment('@return array|null')
            ->setBody('return is_null($value) ? null : json_decode($value, true);')
            ->setPrivate()
            ->addParameter('value');
    }

    private function addJsonExtract(ClassType $class)
    {
        $class
            ->addMethod('extractJson')
            ->addComment('json => string')
            ->addComment('@param array|null $value')
            ->addComment('@return string|null')
            ->setBody('return is_null($value) ? null : json_encode($value);')
            ->setPrivate()
            ->addParameter('value');
    }

    private function addDateHydrate(ClassType $class)
    {
        $class
            ->addMethod('createDate')
            ->addComment('Создать объект даты')
            ->addComment('@param string|null $value')
            ->addComment('@return DateTimeInterface|null')
            ->setBody('return $value ? new DateTimeImmutable($value) : null;')
            ->setPrivate()
            ->addParameter('value');
    }

    private function addDateExtract(ClassType $class)
    {
        $class
            ->addMethod('extractDate')
            ->addComment('Извлечь дату')
            ->addComment('@param DateTimeInterface|null $value')
            ->addComment('@return string|null')
            ->setBody('return $value ? $value->format("Y-m-d") : null;')
            ->setPrivate()
            ->addParameter('value');
    }

    private function addDateTimeHydrate(ClassType $class)
    {
        $class
            ->addMethod('createDateTime')
            ->addComment('Создать объект даты со временем')
            ->addComment('@param string|null $value')
            ->addComment('@return DateTimeInterface|null')
            ->setBody('return $value ? new DateTimeImmutable($value) : null;')
            ->setPrivate()
            ->addParameter('value');
    }

    private function addDateTimeExtract(ClassType $class)
    {
        $class
            ->addMethod('extractDateTime')
            ->addComment('Извлечь дату со временем')
            ->addComment('@param DateTimeInterface|null $value')
            ->addComment('@return string|null')
            ->setBody('return $value ? $value->format("Y-m-d H:i:s") : null;')
            ->setPrivate()
            ->addParameter('value');
    }

    private function calculateUserTypes($properties)
    {
        $this->usedTypes = [];

        foreach ($properties as $data) {
            $this->usedTypes[] = $data[1];
        }

        $this->usedTypes = array_unique($this->usedTypes);
    }

}
