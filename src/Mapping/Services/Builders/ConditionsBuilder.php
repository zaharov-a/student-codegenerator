<?php

declare(strict_types=1);

namespace Ox3a\CodeGenerators\Mapping\Services\Builders;

use Nette\PhpGenerator\ClassType;
use Nette\PhpGenerator\PhpFile;
use Nette\PhpGenerator\PhpNamespace;
use Ox3a\CodeGenerators\Services\Settings\SettingsInterface;

class ConditionsBuilder
{
    /**
     * @var SettingsInterface
     */
    private $settings;

    /**
     * @param SettingsInterface $settings
     */
    public function __construct(SettingsInterface $settings)
    {
        $this->settings = $settings;
    }

    public function build($properties, $namespace, $shortName)
    {
        $namespaceObject = new PhpNamespace($namespace);

        $file = new PhpFile();

        $file->addComment("auto generated");

        $class = $namespaceObject->addClass($shortName);

        $class->addComment("Class {$shortName}")
            ->addComment("@package {$namespace}");

        $file->addNamespace($namespaceObject);

        $this->addUses($namespaceObject)
            ->addProperties($class, $properties)
            ->addPropertyMethods($class, $properties)
            ->addExtraOrderMethods($class)
            ->addExtraConditionsMethods($class)
            ->addExtraLimitMethods($class);

        return $file;
    }

    private function addUses(PhpNamespace $namespaceObject)
    {
        $namespaceObject->addUse($this->settings->getNamespace('mappingConditions'));

        return $this;
    }

    private function addProperties(ClassType $class, array $properties)
    {
        $class->addProperty('order')
            ->setPrivate()
            ->addComment('Порядок сортировки')
            ->addComment('@var array<non-empty-string, non-empty-string>')
            ->setValue([]);

        $class->addProperty('limit')
            ->setPrivate()
            ->addComment('Ограничение')
            ->addComment('@var int|null');

        $class->addProperty('offset')
            ->setPrivate()
            ->addComment('Пропуск')
            ->addComment('@var int')
            ->setValue(0);

        $class->addProperty('conditions')
            ->setPrivate()
            ->addComment('Список условий по полям')
            ->addComment($this->buildConditionsComment($properties))
            ->setValue([]);

        $class->addProperty('extraConditions')
            ->setPrivate()
            ->addComment('Список дополнительных условий')
            ->addComment(sprintf('@var array{%s, string}[]', $class->getName()))
            ->setValue([]);

        return $this;
    }

    private function addPropertyMethods(ClassType $class, $properties)
    {
        $classFullName = $class->getNamespace()->getName() . '\\' . $class->getName();
        $namespace     = $this->settings->getNamespace('mappingConditions');

        foreach ($properties as $field) {
            $class->addMethod(sprintf('get%s', ucfirst($field['name'])))
                ->setReturnType($this->settings->processReturnTypeDeclaration("{$namespace}\\{$field['class']}"))
                ->addComment("@return Conditions\\{$field['class']}")
                ->addBody("if (!isset(\$this->conditions['{$field['name']}'])) {")
                ->addBody(
                    sprintf(
                        '    $this->conditions["%s"] = new Conditions\%s("%s");',
                        $field['name'],
                        $field['class'],
                        $field['for_select']
                    )
                )
                ->addBody('}')
                ->addBody("return \$this->conditions['{$field['name']}'];");

            $class->addMethod(sprintf('orderBy%s', ucfirst($field['name'])))
                ->addComment('@param non-empty-string $direction')
                ->addComment('@return $this')
                ->addBody('if ($direction) {')
                ->addBody(sprintf('    $this->order["%s"] = $direction;', $field['for_order']))
                ->addBody('} else {')
                ->addBody(sprintf('    unset($this->order["%s"]);', $field['for_order']))
                ->addBody('}')
                ->addBody('return $this;')
                ->setReturnType($this->settings->processReturnTypeDeclaration($classFullName))
                ->addParameter('direction')
                ->setDefaultValue('asc');
        }

        return $this;
    }

    private function addExtraOrderMethods(ClassType $class)
    {
        $classFullName         = $class->getNamespace()->getName() . '\\' . $class->getName();
        $returnTypeDeclaration = $this->settings->isReturnTypeDeclaration();

        $class->addMethod('clearOrder')
            ->addComment('Очистить сортировку')
            ->addComment('@return $this')
            ->setReturnType($this->settings->processReturnTypeDeclaration($classFullName))
            ->addBody('$this->order = [];')
            ->addBody('return $this;');

        $class->addMethod('getOrder')
            ->addComment('Получить сортировку')
            ->addComment('@return array<non-empty-string, non-empty-string>')
            ->setReturnType($returnTypeDeclaration ? 'array' : null)
            ->addBody('return $this->order;');

        return $this;
    }

    private function addExtraConditionsMethods(ClassType $class)
    {
        $classFullName = $class->getNamespace()->getName() . '\\' . $class->getName();

        $addConditions = $class->addMethod('addConditions')
            ->addBody('$this->extraConditions[] = [$conditions, $mode];')
            ->addBody('return $this;')
            ->setReturnType($this->settings->processReturnTypeDeclaration($classFullName))
            ->addComment('Добавить дополнительные условия')
            ->addComment(sprintf('@param %s $conditions', $class->getName()))
            ->addComment('@param string $mode')
            ->addComment('@return $this');

        $addConditions->addParameter('conditions')
            ->setType($class->getNamespace()->getName() . '\\' . $class->getName());
        $addConditions->addParameter('mode')
            ->setDefaultValue('AND');

        $class->addMethod('getConditions')
            ->setReturnType($this->settings->processReturnTypeDeclaration('array'))
            ->setBody(
                sprintf(
                    '
$conditions = [];

foreach ($this->conditions as $condition) {
    if (($condition = $condition->getCondition())) {
        $conditions[] = [$condition, "AND"];
    }
}

foreach ($this->extraConditions as $extraConditions) {
    %s = $extraConditions;
    if (($extraConditions = $extraConditions->getConditions())) {
        $conditions[] = [$extraConditions, $mode];
    }
}

return $conditions;
',
                    $this->settings->getListUnpacking('$extraConditions, $mode')
                )

            )
            ->addComment('Получить дерево условий')
            ->addComment('@return list<array{mixed, string}>');

        return $this;
    }

    private function addExtraLimitMethods(ClassType $class)
    {
        $classFullName = $class->getNamespace()->getName() . '\\' . $class->getName();

        $class->addMethod('setLimit')
            ->addComment('Получить limit')
            ->addComment('@param int|null $limit')
            ->addComment('@return $this')
            ->setReturnType($this->settings->processReturnTypeDeclaration($classFullName))
            ->addBody('$this->limit = $limit;')
            ->addBody('return $this;')
            ->addParameter('limit');

        $class->addMethod('getLimit')
            ->addComment('Получить limit')
            ->addComment('@return int|null')
            ->setReturnType($this->settings->processReturnTypeDeclaration('?int'))
            ->addBody('return $this->limit;');

        $class->addMethod('setOffset')
            ->addComment('Получить offset')
            ->addComment('@param int $offset')
            ->addComment('@return $this')
            ->setReturnType($this->settings->processReturnTypeDeclaration($classFullName))
            ->addBody('$this->offset = $offset;')
            ->addBody('return $this;')
            ->addParameter('offset');

        $class->addMethod('getOffset')
            ->addComment('Получить offset')
            ->addComment('@return int')
            ->setReturnType($this->settings->processReturnTypeDeclaration('int'))
            ->addBody('return $this->offset;');
    }

    private function buildConditionsComment(array $properties): string
    {
        $comment   = "@var array{\n";
        foreach ($properties as $field) {
            $type = "Conditions\\{$field['class']}";
            $id   = $field['name'];

            $comment .= "         {$id}?: {$type},\n";
        }

        $comment .= '     }';
        return $comment;
    }
}
