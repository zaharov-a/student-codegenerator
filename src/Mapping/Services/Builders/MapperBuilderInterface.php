<?php

namespace Ox3a\CodeGenerators\Mapping\Services\Builders;

use Nette\PhpGenerator\PhpFile;

interface MapperBuilderInterface
{
    public function build(array $mapper, array $entity, array $hydrator, array $conditions): PhpFile;
}
