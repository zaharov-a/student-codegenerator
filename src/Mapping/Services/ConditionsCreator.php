<?php
/**
 * {Template_Description_Abstract}
 *
 * @author    Zakharov.A
 * @date      21.12.2020
 * @copyright {Template_Description_Copyrights}
 */

namespace Ox3a\CodeGenerators\Mapping\Services;

use Nette\PhpGenerator\PsrPrinter;
use Ox3a\Annotation\Analyzer;
use Ox3a\Annotation\Mapping\Column;
use Ox3a\Annotation\Mapping\Table;
use Ox3a\CodeGenerators\Mapping\Services\Builders\ConditionsBuilder;
use ReflectionClass;

class ConditionsCreator
{

    /**
     * @var ReflectionClass
     */
    private $class;

    /**
     * Карта полей
     * @var array
     */
    private $mapData = [];

    /**
     * Список используемых типов в гидраторе
     * @var array
     */
    public $usedTypes = [];

    private $entitiesNamespace = 'Models';
    private $entitySuffix      = 'Model';

    private $conditionsNamespace = 'Repositories\\Db';
    private $conditionsSuffix    = 'Conditions';

    /**
     * @var ConditionsBuilder
     */
    private $builder;

    /**
     * @param ConditionsBuilder $builder
     */
    public function __construct(ConditionsBuilder $builder)
    {
        $this->builder = $builder;
    }

    public function getConditionsDir(string $entityPath)
    {
        return dirname(
                str_replace($this->entitiesNamespace, str_replace('\\', '/', $this->conditionsNamespace), $entityPath)
            )
            . '/' . $this->class->getShortName();
    }

    public function getNamespace()
    {
        return str_replace(
                $this->entitiesNamespace,
                $this->conditionsNamespace,
                $this->class->getNamespaceName()
            )
            . '\\' . $this->class->getShortName();
    }

    public function create($className)
    {
        $this->init($className);

        $file = $this->builder->build($this->mapData, $this->getNamespace(), $this->getShortClassName());

        $printer = new PsrPrinter();

        return [
            $this->getShortClassName(),
            $printer->printFile($file),
        ];
    }

    private function init($className)
    {
        $this->class = new ReflectionClass($className);

        $this->mapData   = [];
        $this->usedTypes = [];

        $annotations = $this->getAnnotations();

        $table     = $annotations['class'][Table::class][0];
        $mainTable = $table['alias'] ?: $table['name'];

        foreach ($annotations['properties'] as $property => $list) {
            if (!isset($list[Column::class])) {
                continue;
            }
            $column = $list[Column::class][0];

            if (($prefix = ($column['table'] ?? $mainTable))) {
                $prefix = "{$prefix}.";
            }
            if (empty($column['get']) && isset($column['expr'])) {
                $column['get'] = $column['expr'];
            }
            $this->mapData[$property] = [
                'name'       => $property,
                'for_select' => empty($column['get']) ? ($prefix . $column['name']) : $column['get'],
                'for_order'  => $property,
                'class'      => ucfirst($column['type']) . 'Condition',
            ];

            $this->usedTypes[] = $column['type'];
        }

        $this->usedTypes = array_unique($this->usedTypes);
    }

    private function getAnnotations()
    {
        $analyzer = new Analyzer();
        return $analyzer->run($this->class->getName());
    }

    public function getShortClassName()
    {
        return str_replace(
            $this->entitySuffix,
            $this->conditionsSuffix,
            $this->class->getShortName()
        );
    }
}
