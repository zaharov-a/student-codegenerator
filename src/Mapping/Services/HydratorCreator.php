<?php
/**
 * {Template_Description_Abstract}
 *
 * @author    Zakharov.A
 * @date      21.12.2020
 * @copyright {Template_Description_Copyrights}
 */

namespace Ox3a\CodeGenerators\Mapping\Services;

use Nette\PhpGenerator\PsrPrinter;
use Ox3a\Annotation\Analyzer;
use Ox3a\Annotation\Mapping\Column;
use Ox3a\Annotation\Mapping\Viewonly;
use Ox3a\CodeGenerators\Mapping\Services\Builders\HydratorBuilder;
use ReflectionClass;

class HydratorCreator
{

    /**
     * @var ReflectionClass
     */
    private $class;

    /**
     * Карта полей
     * @var array
     */
    private $mapData = [];

    /**
     * Список полей только для чтения
     * @var string[]
     */
    private $readonly = [];

    /**
     * Список используемых типов в гидраторе
     * @var array
     */
    public $usedTypes = [];

    private $entitiesNamespace = 'Models';
    private $entitySuffix      = 'Model';

    private $hydratorsNamespace = 'Repositories\\Db';
    private $hydratorSuffix     = 'Hydrator';

    /**
     * @var HydratorBuilder
     */
    private $builder;

    /**
     * @var bool
     */
    private $isViewonly;

    /**
     * @param HydratorBuilder $builder
     */
    public function __construct(HydratorBuilder $builder)
    {
        $this->builder = $builder;
    }

    public function getHydratorsDir(string $entityPath)
    {
        return dirname(
                str_replace($this->entitiesNamespace, str_replace('\\', '/', $this->hydratorsNamespace), $entityPath)
            )
            . '/' . $this->class->getShortName();
    }

    public function getNamespace()
    {
        return str_replace(
                $this->entitiesNamespace,
                $this->hydratorsNamespace,
                $this->class->getNamespaceName()
            )
            . '\\' . $this->class->getShortName();
    }

    public function create($className)
    {
        $this->init($className);

        $file = $this->builder->build(
            $this->mapData,
            $this->readonly,
            [
                'namespace' => $this->getNamespace(),
                'shortName' => $this->getShortClassName(),
                'class'     => $this->getNamespace() . '\\' . $this->getShortClassName(),
                'isViewonly' => $this->isViewonly,
            ],
            [
                'class' => $className,
                'shortName' => $this->class->getShortName(),
            ]
        );

        $printer = new PsrPrinter();

        return [
            $this->getShortClassName(),
            $printer->printFile($file),
        ];
    }

    private function init($className)
    {
        $this->class = new ReflectionClass($className);

        $this->mapData = [];

        $annotations = $this->getAnnotations();
        foreach ($annotations['properties'] as $property => $list) {
            if (!isset($list[Column::class])) {
                continue;
            }
            $column = $list[Column::class][0];

            $this->mapData[$property] = [$column['name'], $column['type']];

            $readonlyClass = 'Ox3a\Annotation\Mapping\Readonly';
            if (isset($list[Viewonly::class]) || isset($list[$readonlyClass])) {
                $this->readonly[] = $property;
            }
        }

        $this->isViewonly = isset($annotations['class'][Viewonly::class]);
    }

    private function getAnnotations()
    {
        $analyzer = new Analyzer();
        return $analyzer->run($this->class->getName());
    }

    public function getShortClassName()
    {
        return str_replace(
            $this->entitySuffix,
            $this->hydratorSuffix,
            $this->class->getShortName()
        );
    }

}
