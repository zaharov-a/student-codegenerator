<?php
/**
 * {Template_Description_Abstract}
 *
 * @author    Zakharov.A
 * @date      17.12.2020
 * @copyright {Template_Description_Copyrights}
 */

namespace Ox3a\CodeGenerators\Mapping\Services;

use Ox3a\Annotation\Analyzer;
use Ox3a\Annotation\Mapping\Column;
use Ox3a\Annotation\Mapping\Id;
use Ox3a\Annotation\Mapping\Join;
use Ox3a\Annotation\Mapping\LeftJoin;
use Ox3a\Annotation\Mapping\Table;
use Ox3a\Annotation\Mapping\Viewonly;
use Ox3a\CodeGenerators\Mapping\Services\Builders\MapperBuilderInterface;
use Ox3a\CodeGenerators\Services\PhpGenerator\PsrPrinter;
use ReflectionClass;

class MapperCreator
{
    /**
     * @var array|array[]
     */
    private $annotations;

    /**
     * @var ReflectionClass
     */
    private $class;

    private $entitiesNamespace = 'Models';
    private $entitySuffix = 'Model';

    private $mappersNamespace = 'Repositories\\Db';
    private $mapperSuffix = 'Mapper';

    private $hydratorsNamespace = 'Repositories\\Db';
    private $hydratorSuffix = 'Hydrator';

    private $conditionsNamespace = 'Repositories\\Db';
    private $conditionsSuffix = 'Conditions';

    /**
     * @var MapperBuilderInterface
     */
    private $builder;

    /**
     * @param MapperBuilderInterface $builder
     */
    public function __construct(MapperBuilderInterface $builder)
    {
        $this->builder = $builder;
    }

    public function getMappersDir(string $entityPath)
    {
        return dirname(
                str_replace($this->entitiesNamespace, str_replace('\\', '/', $this->mappersNamespace), $entityPath)
            )
            . '/' . $this->class->getShortName();
    }

    public function create(string $className)
    {
        $this->init($className);

        $file = $this->builder->build(
            $mapper = $this->getMapper(),
            $this->getEntity(),
            $this->getHydrator(),
            $this->getConditions()
        );

        $printer = new PsrPrinter();

        return [
            $mapper['shortName'],
            $printer->printFile($file),
        ];
    }

    private function init($className)
    {
        $this->class = new ReflectionClass($className);
    }

    private function getAnnotations()
    {
        if (!$this->annotations) {
            $analyzer          = new Analyzer();
            $this->annotations = $analyzer->run($this->class->getName());
        }
        return $this->annotations;
    }

    private function getTable(array $annotations)
    {
        return $annotations['class'][Table::class][0];
    }

    private function getPrimaryKey(array $annotations)
    {
        $properties = $annotations['properties'];
        foreach ($properties as $property) {
            if (isset($property[Id::class])) {
                return $property[Column::class][0]['name'];
            }
        }
        return '';
    }

    private function getJoins($type)
    {
        $annotations = $this->getAnnotations();
        $joins       = $annotations['class'][$type] ?? [];

        $result = [];

        foreach ($joins as $join) {
            $join['columns'] = $this->getTableColumns($join['alias'] ?: $join['table']);
            $result[]        = $join;
        }

        return $result;
    }

    private function getTableColumns($name)
    {
        $annotations = $this->getAnnotations();
        $cols        = [];
        foreach ($annotations['properties'] as $property => $list) {
            if (($col = $list[Column::class] ?? false)) {
                $col = $col[0];
                if ($col['table'] == $name) {
                    $cols[] = [
                        'alias' => $property,
                        'name'  => $col['name'],
                        'get'   => $col['expr'] ?? $col['get'] ?? null,
                        'set'   => $col['set'] ?? null,
                    ];
                }
            }
        }

        return $cols;
    }

    private function getMapper(): array
    {
        $shortName = str_replace($this->entitySuffix, $this->mapperSuffix, $this->class->getShortName());
        $namespace = str_replace(
                $this->entitiesNamespace,
                $this->mappersNamespace,
                $this->class->getNamespaceName()
            )
            . '\\' . $this->class->getShortName();

        return [
            'namespace'  => $namespace,
            'shortName'  => $shortName,
            'class'      => $namespace . '\\' . $shortName,
            'isViewonly' => $this->isViewonly(),
        ];
    }

    private function getEntity(): array
    {
        $concrete = null;
        $table    = $this->getTable($this->getAnnotations());
        $columns  = array_merge(
            $this->getTableColumns(null),
            $this->getTableColumns($table['name'])
        );

        if (($alias = $table['alias'])) {
            $columns = array_merge($columns, $this->getTableColumns($alias));
        }
        return [
            'namespace'   => $this->class->getNamespaceName(),
            'shortName'   => $this->class->getShortName(),
            'class'       => $this->class->getName(),
            'concrete'    => $concrete,
            'table'       => $table + ['columns' => $columns],
            'joins'       => $this->getJoins(Join::class),
            'leftJoins'   => $this->getJoins(LeftJoin::class),
            'primaryKey'  => $this->getPrimaryKey($this->getAnnotations()),
            'isDeletable' => $this->class->hasMethod('isDelete'),
        ];
    }

    private function getHydrator()
    {
        $shortName = str_replace($this->entitySuffix, $this->hydratorSuffix, $this->class->getShortName());
        $namespace = str_replace(
                $this->entitiesNamespace,
                $this->hydratorsNamespace,
                $this->class->getNamespaceName()
            )
            . '\\' . $this->class->getShortName();;

        return [
            'namespace' => $namespace,
            'shortName' => $shortName,
            'class'     => $namespace . '\\' . $shortName,
        ];
    }

    private function getConditions()
    {
        $shortName = str_replace($this->entitySuffix, $this->conditionsSuffix, $this->class->getShortName());
        $namespace = str_replace(
                $this->entitiesNamespace,
                $this->conditionsNamespace,
                $this->class->getNamespaceName()
            )
            . '\\' . $this->class->getShortName();;

        return [
            'namespace' => $namespace,
            'shortName' => $shortName,
            'class'     => $namespace . '\\' . $shortName,
        ];
    }

    /**
     * @return bool
     */
    private function isViewonly()
    {
        return isset($this->getAnnotations()['class'][Viewonly::class]);
    }
}
