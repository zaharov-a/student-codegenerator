<?php
/**
 * {Template_Description_Abstract}
 *
 * @author    Zakharov.A
 * @date      11.02.2021
 * @copyright {Template_Description_Copyrights}
 */

namespace Ox3a\CodeGenerators\Services;


use RuntimeException;

class PrintService
{
    private $indent = 4;


    /**
     * @param int $indent
     */
    public function setIndent(int $indent): void
    {
        $this->indent = $indent;
    }


    /**
     * @param mixed $value
     * @return string
     */
    public function print($value): string
    {
        if (is_array($value)) {
            return $this->printArray($value);
        }

        if (is_float($value) || is_int($value)) {
            return $value;
        }

        if (is_null($value)) {
            return 'null';
        }

        return sprintf('"%s"', $value);
    }


    public function printArray(array $array, $level = 0): string
    {
        $rows         = [];
        $rows[]       = $this->appendIndent('[', $level);
        $isIndexArray = ($array === array_values($array));
        foreach ($array as $key => $value) {
            $row = $this->appendIndent('', $level + 1);
            if (!$isIndexArray) {
                $row .= is_string($key) ? sprintf("'%s'", $key) : $key;
                $row .= ' => ';
            }
            if (is_array($value)) {
                $row .= trim($this->printArray($value, $level + 1));
            } elseif (is_object($value)) {
                if (!method_exists($value, '__toString')) {
                    throw new RuntimeException('Объекты не поддерживаются');
                }
                $row .= (string)$value;
            } else {
                $row .= json_encode($value, JSON_UNESCAPED_UNICODE);
            }
            $row    .= ',';
            $rows[] = $row;
        }
        $rows[] = $this->appendIndent(']', $level);
        $rows[] = '';

        return implode(PHP_EOL, $rows);
    }


    public function appendIndent($string, $level)
    {
        return ($level ? str_repeat(' ', $level * $this->indent) : '') . $string;
    }

}
