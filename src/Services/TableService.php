<?php
/**
 * {Template_Description_Abstract}
 *
 * @author    Zakharov.A
 * @date      16.12.2020
 * @copyright {Template_Description_Copyrights}
 */

namespace Ox3a\CodeGenerators\Services;


use Laminas\Db\TableGateway\TableGateway;
use Ox3a\Service\DbServiceInterface;

class TableService
{
    /**
     * @var DbServiceInterface
     */
    private $dbService;


    /**
     * TableService constructor.
     * @param $dbService
     */
    public function __construct($dbService)
    {
        $this->dbService = $dbService;
    }


    public function get($table)
    {
        return new TableGateway($table, $this->dbService->getAdapter());
    }
}
