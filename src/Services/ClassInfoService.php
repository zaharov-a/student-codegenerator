<?php
/**
 * {Template_Description_Abstract}
 *
 * @author    Zakharov.A
 * @date      24.10.2021
 * @copyright {Template_Description_Copyrights}
 */

namespace Ox3a\CodeGenerators\Services;

class ClassInfoService
{
    public function getClassName(string $fileName): ?string
    {
        $code      = file_get_contents($fileName);
        $tokens    = token_get_all($code);
        $namespace = null;

        do {
            $token = current($tokens);
            switch (is_array($token) ? $token[0] : $token) {
                case T_NAMESPACE:
                    $take = [T_STRING, T_NS_SEPARATOR];
                    if (version_compare(PHP_VERSION, '8.0.0', '>=')) {
                        $take[] = T_NAME_QUALIFIED;
                        $take[] = T_NAME_FULLY_QUALIFIED;
                    }

                    $namespace = ltrim($this->fetch($tokens, $take) . '\\', '\\');
                    break;

                case T_CLASS:
                case T_INTERFACE:
                    if ($name = $this->fetch($tokens, [T_STRING])) {
                        return $namespace . $name;
                    }
                    break;
            }
        } while (next($tokens));

        return null;
    }

    private function fetch(&$tokens, $take)
    {
        $res = null;
        while ($token = next($tokens)) {
            [$token, $s] = is_array($token) ? $token : [$token, $token];
            if (in_array($token, $take, true)) {
                $res .= $s;
            } elseif (!in_array($token, [T_DOC_COMMENT, T_WHITESPACE, T_COMMENT], true)) {
                break;
            }
        }

        return $res;
    }
}
