<?php
/**
 * {Template_Description_Abstract}
 *
 * @author    Zakharov.A
 * @date      17.10.2021
 * @copyright {Template_Description_Copyrights}
 */

namespace Ox3a\CodeGenerators\Services\Settings;

class PskSettings implements SettingsInterface
{

    protected $classList = [
        'dbInterface'               => 'Ox3a\Service\DbServiceInterface',
        'sqlSelect'                 => 'Zend\Db\Sql\Select',
        'sqlExpression'             => 'Zend\Db\Sql\Predicate\Expression',
        'sqlExpressionShort'        => 'Expression',
        'mappingConditionBuilder'   => 'Ox3a\Core\ConditionsBuilder\ConditionsBuilder',
        'mappingTableService'       => '',
        'mappingTableAbstract'      => 'Zend\Db\TableGateway\TableGateway',
        'mappingTableAbstractShort' => 'TableGateway',
    ];

    protected $namespaceList = [
        'mappingConditions' => 'Ox3a\Core\ConditionsBuilder\Conditions',
    ];

    public function processReturnTypeDeclaration(string $type): ?string
    {
        return null;
    }

    public function getClass(string $class): string
    {
        return $this->classList[$class];
    }

    public function getNamespace(string $namespace): string
    {
        return $this->namespaceList[$namespace];
    }

    public function isReturnTypeDeclaration(): bool
    {
        return false;
    }

    public function getListUnpacking(string $content): string
    {
        return sprintf('list(%s)', $content);
    }

}
