<?php
/**
 * {Template_Description_Abstract}
 *
 * @author    Zakharov.A
 * @date      17.10.2021
 * @copyright {Template_Description_Copyrights}
 */

namespace Ox3a\CodeGenerators\Services\Settings;

interface SettingsInterface
{
    public function getClass(string $class): string;

    public function getNamespace(string $namespace): string;

    public function isReturnTypeDeclaration(): bool;

    public function processReturnTypeDeclaration(string $type): ?string;

    public function getListUnpacking(string $content): string;
}
