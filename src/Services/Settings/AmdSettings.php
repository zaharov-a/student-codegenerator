<?php
/**
 * {Template_Description_Abstract}
 *
 * @author    Zakharov.A
 * @date      17.10.2021
 * @copyright {Template_Description_Copyrights}
 */

namespace Ox3a\CodeGenerators\Services\Settings;

class AmdSettings extends PskSettings
{

    protected $classList = [
        'dbInterface'               => 'Amd\AmdCoreServices\Db\DbInterface',
        'sqlSelect'                 => 'Zend_Db_Select',
        'sqlExpression'             => 'Zend_Db_Expr',
        'sqlExpressionShort'        => 'Zend_Db_Expr',
        'mappingConditionBuilder'   => 'Amd\AmdCoreServices\ConditionsBuilder\ConditionsBuilder',
        'mappingTableService'       => '',//'Amd\AmdCoreServices\Db\Zf1TablesService',
        'mappingTableAbstract'      => 'Zend_Db_Table_Abstract',
        'mappingTableAbstractShort' => 'Zend_Db_Table_Abstract',
    ];

    protected $namespaceList = [
        'mappingConditions' => 'Amd\AmdCoreServices\ConditionsBuilder\Conditions',
    ];

    public function isReturnTypeDeclaration(): bool
    {
        return true;
    }

    public function processReturnTypeDeclaration(string $type): ?string
    {
        return $type;
    }

    public function getListUnpacking(string $content): string
    {
        return sprintf('[%s]', $content);
    }
}
