<?php

namespace Ox3a\CodeGenerators\Services\PhpGenerator;

use Nette\PhpGenerator\Printer;

class PsrPrinter extends Printer
{

    /** @var string */
    protected $indentation = '    ';

    /** @var int */
    protected $linesBetweenMethods = 1;

    public function __construct()
    {
        parent::__construct();
        $this->dumper = new Dumper();
    }

}
