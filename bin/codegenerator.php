#!/usr/bin/env php
<?php

if (!defined('APP_VERSION')) {
    define('APP_VERSION', 'development');
}

use Composer\Autoload\ClassLoader;
use League\Container\Container;
use League\Container\ReflectionContainer;
use Ox3a\CodeGenerators\Mapping\Commands as Mapping;
use Ox3a\CodeGenerators\Form\Commands as Form;
use Ox3a\CodeGenerators\Controllers\Commands as Controller;
use Ox3a\CodeGenerators\Services\CommandLoader;
use Ox3a\Service\ConfigService;
use Psr\Container\ContainerInterface;
use Symfony\Component\Console\Application;

$phpLoader = require __DIR__ . '/../vendor/autoload.php';

$envFile = getcwd() . '/.env';
if (!is_file($envFile)) {
    throw new RuntimeException('Не найден файл .env с настройками');
}

$env = file($envFile);
foreach ($env as $value) {
    $value = trim($value);
    if (substr($value, 0, 1) != '#') {
        putenv($value);
    }
}

$settingsType = getenv('CODEGENERATOR_SETTINGS');
if (!$settingsType) {
    throw new RuntimeException('Не указан тип генерации CODEGENERATOR_SETTINGS');
}

$container = new Container();
$container->delegate(new ReflectionContainer(true));
$container->defaultToShared();
$container->add(ContainerInterface::class, $container);
$container->add(ClassLoader::class, $phpLoader);

$autoload = getcwd() . '/vendor/composer/autoload_psr4.php';
if (is_file($autoload)) {
    $map = require $autoload;
    if (($baseNamespaces = getenv('CODEGENERATOR_BASE_NAMESPACE'))) {
        $baseNamespaces = explode(',', $baseNamespaces);

        foreach ($map as $namespace => $path) {
            $load = false;
            foreach ($baseNamespaces as $baseNamespace) {
                if (strpos($namespace, $baseNamespace) === 0) {
                    $load = true;
                    break;
                }
            }
            if ($load) {
                $phpLoader->setPsr4($namespace, $path);
            }
        }
    }
}

/** @var CommandLoader $commandLoader */
$commandLoader = $container->get(CommandLoader::class);

$servicesFile = __DIR__ . '/../configs/containers/' . $settingsType . '.services.php';

if (!is_file($servicesFile)) {
    throw new RuntimeException('Не известный тип генерации CODEGENERATOR_SETTINGS');
}

require_once $servicesFile;

$configService = ConfigService::getInstance();
$configService->addDir(__DIR__ . '/../configs');

$console = new Application('0x3a/code-generator', APP_VERSION);

$console->setCommandLoader($commandLoader);

$commandLoader->add(Mapping\ExtractEntityCommand::class);
$commandLoader->add(Mapping\FullCommand::class);
$commandLoader->add(Mapping\MapperCommand::class);
$commandLoader->add(Mapping\HydratorCommand::class);
$commandLoader->add(Mapping\ConditionsCommand::class);
$commandLoader->add(Form\CreateCommand::class);
$commandLoader->add(Controller\RoutesCommand::class);

$console->run();
